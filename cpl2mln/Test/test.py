#
# Script principale, esso avvia la generazione dei test su Cplint e Alchemy
#
 
import woman_data_generator, translate_cpl2mln, exec_test, data_mining, os, sys, test_utility, textwrap
args = sys.argv
go = True
see_par = True
if len(args) == 6:
	test_utility.alchemy_home = args[1]
	test_utility.translator_home = args[2]
	test_utility.query_cplint = args[3]
	test_utility.query_alchemy = args[4]
	test_utility.alchemy_empty_db_file_txt = args[5]
elif len(args) == 5:
	test_utility.alchemy_home = args[1]
	test_utility.translator_home = args[2]
	test_utility.query_cplint = args[3]
	test_utility.query_alchemy = args[4]
elif len(args) == 3:
	test_utility.alchemy_home = args[1]
	test_utility.translator_home = args[2]
elif len(args) == 1:
	print 'nessun parametro'
	see_par = False
	
else:
	answ = '''Errore parametri non corretti, arg 0 = alchemy_home, arg 1 = translator_home, arg 2 = query in formato cplint,	arg 3 = query in formato alchemy, gli argomenti 2 e 3 se mancano vengono utilizzate le query di default	'''
	print textwrap.fill(answ, width=70)
	go = False
	see_par = False
	
if see_par:
	print 'parametri cambiati'
	print len(args)
	print args[0]
	print test_utility.alchemy_home , args[1]
	print test_utility.translator_home , args[2]
	print test_utility.query_cplint , args[3]
	print test_utility.query_alchemy , args[4]
	print test_utility.alchemy_empty_db_file_txt , args[5]

if go:	
	print 'esecuzione test su Cplint e Alchemy iniziata'
	woman_data_generator.test_gen()
	translate_cpl2mln.translate()
	exec_test.exe_test(3)
	data_mining.write_cvs_test_result(os.getcwd()+'/data_1.cvs', data_mining.get_info(),1)
	data_mining.write_cvs_test_result(os.getcwd()+'/data_2.cvs', data_mining.get_info(),2)
	print 'esecuzione test su Cplint e Alchemy terminata'