#
# Semplice script che crea a piacimento un numero di fatti woman in
# in 2 file *.cpl e *.uni per poter creare velocemente tutte le casistiche 
# necessarie per i test
#

import string
import os
import shutil
import test_utility



#funzioni
def create_woman_test_dir_names(top_dir, place_list, places_dir_def, woman_list, womans_dir_def, cplint_dir, cplint_list, alchemy_dir, alchemy_list):
	"""funzione che crea la lista contenente tutti i percorsi da generare,	PARAMETRI: place_list, woman_list, cplint_list, alchemy_list
	"""
	#creo la lista con le directory di cplint
	L =[(os.getcwd(),"%s" %top_dir,"%d%s" %(pl,places_dir_def),"%d%s" %(wo,womans_dir_def),"%s"%cplint_dir,cp) for pl in place_list for wo in woman_list for cp in cplint_list]
	#espando la sista con le directory di alchemy
	L.extend((os.getcwd(),"%s" %top_dir,"%d%s" %(pl,places_dir_def),"%d%s" %(wo,womans_dir_def),"%s"%alchemy_dir,al) for pl in place_list for wo in woman_list for al in alchemy_list)
	return L

def create_woman_test_keys_tuples(place_list, woman_list, cplint_dir, cplint_list, alchemy_dir, alchemy_list):
	"""funzione che crea la lista contenente tutti i percorsi da generare,	PARAMETRI: place_list, woman_list, cplint_list, alchemy_list
	"""
	#creo la lista con le directory di cplint
	L =[(pl,wo,cplint_dir,cp) for pl in place_list for wo in woman_list for cp in cplint_list]
	#espando la sista con le directory di alchemy
	L.extend((pl,wo,alchemy_dir,al) for pl in place_list for wo in woman_list for al in alchemy_list)
	return L

def join_dir2path(path_elems):
	path_dir2create = ''
	for el in path_elems:
		path_dir2create = os.path.join(path_dir2create,el) #compongo il percorso della directory
	return path_dir2create
	
def create_test_dir(path_list):
	""" funzione che crea il direttorio leggendo una lista di percorsi PARAMETRI: path_list lista di percorsi
	"""
	for pa in path_list:		
		path_dir2create = join_dir2path(pa)
		os.makedirs(path_dir2create)

def make_woman_cpl_file(womanList, rules):
	""" crea il file cpl in base al numero di woman
	"""
	testo = "/*womans*/\n"
	for x in womanList:
		testo +="woman(%s).\n" %x	
	testo += "\n/*rules*/\n"
	testo += rules
	return testo

def make_woman_uni_file(womanList, prefList, placeList):
	""" crea il file cpl in base al numero di woman
	"""
	testo = "/*mode definition*/\n"
	testo += "mode(woman(name)).\nmode(like(name,preference)).\nmode(goesto(name,place)).\n\n /*type  definition*/ \ntype(name,["	
	for x in womanList[0:len(womanList)-1]:
		testo +="%s," %x		
	testo += "%s]).\n" %womanList[len(womanList)-1]
	
	testo += "type(preference,["
	for x in prefList[0:len(prefList)-1]:
		testo +="%s," %x		
	testo += "%s]).\n" %prefList[len(prefList)-1]
	
	testo += "type(place,["
	for x in placeList[0:len(placeList)-1]:
		testo +="%s," %x		
	testo += "%s]).\n\n" %placeList[len(placeList)-1]
	
	testo += "universe(['Name'],["
	for x in womanList[0:len(womanList)-1]:
		testo +="%s," %x		
	testo += "%s]).\n" %womanList[len(womanList)-1]
	return testo
	
def make_woman_list(nWoman):
	""" crea una lista generica di donne di dimensione nWoman
	"""
	return ["w%d" % num for num in xrange(1,nWoman+1)]
		
def test_gen():
	if(os.path.isdir(os.path.join(os.getcwd(),test_utility.top_dir))):
		# cancello se esiste il direttorio dei test	
		try:
			shutil.rmtree(os.path.join(os.getcwd(),test_utility.top_dir))
			#print os.system('rm -r woman_test')
		except IOError, (errno, strerror):
			print "I/O error(%s): %s" % (errno, strerror)
		else:
			print 'inizio la creazione direttorio di test'	
			
	# nomi dei direttori da creare
	dir_names = create_woman_test_dir_names(test_utility.top_dir,test_utility.places, test_utility.places_dir_def, test_utility.womans, test_utility.womans_dir_def, test_utility.cplint_dir, test_utility.cplint_type, test_utility.alchemy_dir, test_utility.alchemy_type)	
	
	# creo il direttorio di test
	create_test_dir(dir_names)	
	
	#creo tutti gli esempi
	for pl in test_utility.places:
		for wo in test_utility.womans:
			# calcolo del percorso del file in dir cplint
			file_path = os.path.join(os.getcwd(), "%s" %test_utility.top_dir, "%d%s" %(pl, test_utility.places_dir_def), "%d%s" %(wo,test_utility.womans_dir_def), "%s"%test_utility.cplint_dir)
			#print file_path
			test_utility.write_file(file_path, test_utility.cpl_file_name , make_woman_cpl_file(make_woman_list(wo), test_utility.rules_txt[test_utility.places.index(pl)]))
			test_utility.write_file(file_path, test_utility.uni_file_name , make_woman_uni_file(make_woman_list(wo), test_utility.pref_list, test_utility.places_list[0:(pl+1)]))
			test_utility.write_file(file_path, test_utility.yap_translation_file_name, test_utility.yap_translation_file_txt)
			test_utility.write_file(file_path, test_utility.yap_execute_tests_cplint_file_lpadsld_name, test_utility.get_yap_execute_tests_cplint_file_lpadsld_txt())
			test_utility.write_file(file_path, test_utility.yap_execute_tests_cplint_file_lpad_name, test_utility.get_yap_execute_tests_cplint_file_lpad_txt())
			test_utility.write_file(file_path, test_utility.yap_execute_tests_cplint_file_cpl_name, test_utility.get_yap_execute_tests_cplint_file_cpl_txt())
			# calcolo del percorso del file in dir alchemy
			file_path = os.path.join(os.getcwd(),"%s" %test_utility.top_dir,"%d%s" %(pl,test_utility.places_dir_def),"%d%s" %(wo,test_utility.womans_dir_def),"%s"%test_utility.alchemy_dir)			
			test_utility.write_file(file_path, test_utility.alchemy_empty_db_file_name, test_utility.alchemy_empty_db_file_txt)
			test_utility.write_file(file_path, test_utility.execute_tests_alchemy_file_name, test_utility.get_execute_tests_alchemy_file_txt(file_path))
	
	
# utilizzo del modulo stand-alone
if __name__ == '__main__':
	print 'esecuzione stand-alone iniziata'
	test_gen()
	print 'esecuzione stand-alone terminata'
