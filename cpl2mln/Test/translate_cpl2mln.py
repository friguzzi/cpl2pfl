#
# Script per che esegue la traduzione dei file cpl in mln
#
#
import os, test_utility

def translate_cpl2mln(path_cmd):
	"""avvia il programma di traduzione prolog
	"""			
	os.spawnl(os.P_WAIT, path_cmd)
	
def translate_filter(f):
	return (f == test_utility.yap_translation_file_name)
		
def translate ():
	test_utility.walktree(os.getcwd(), translate_filter, translate_cpl2mln)
			
#stand alone			
if __name__ == '__main__':
	print 'esecuzione stand-alone iniziata'
	translate()
	print 'esecuzione stand-alone terminata'