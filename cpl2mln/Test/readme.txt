TEST:

1)womans_2places
///////////////////////////////////////////////////////////////
/Woman.cpl base con 2 atomi alla head (scelta tra 2 'places'):/
//////////////////////////////////////////////////////////////

woman(mary).
woman(andrea).
//etc....

like(Name,dance):0.6 ; like(Name,ballet):0.4 :-woman(Name).
goesto(Name,pub):0.4 ; goesto(Name,disco):0.6 :- woman(Name),like(Name,dance).
goesto(Name,pub):0.4 ; goesto(Name,theatre):0.6 :- woman(Name),like(Name,ballet).


poi eseguio il test aumentando il numero di 'woman'con :
10,20,40,60,80,100 'woman'


2)womans_3places
///////////////////////////////////////////////////////////////
/Woman.cpl base con 3 atomi alla head (scelta tra 3 'places'):/
///////////////////////////////////////////////////////////////

woman(mary).
woman(andrea).
//etc....

like(Name,dance):0.6 ;like(Name,ballet):0.4:-woman(Name).
goesto(Name,pub):0.3;goesto(Name,disco):0.5;goesto(Name,restaurant):0.2   :- woman(Name),like(Name,dance).
goesto(Name,pub):0.3;goesto(Name,theatre):0.5;goesto(Name,restaurant):0.2 :- woman(Name),like(Name,ballet).


poi eseguio il test aumentando il numero di 'woman'con :
10,20,40,60,80,100 'woman'


3)womans_4places
///////////////////////////////////////////////////////////////
/Woman.cpl base con 4 atomi alla head (scelta tra 4 'places'):/
///////////////////////////////////////////////////////////////

woman(mary).
woman(andrea).
//etc....


like(Name,dance):0.6 ;like(Name,ballet):0.4:-woman(Name).
goesto(Name,pub):0.2;goesto(Name,disco):0.5;goesto(Name,restaurant):0.2;goesto(Name,home):0.1 :- woman(Name),like(Name,dance).
goesto(Name,pub):0.2;goesto(Name,theatre):0.5;goesto(Name,restaurant):0.2;goesto(Name,home):0.1 :- woman(Name),like(Name,ballet).


poi eseguio il test aumentando il numero di 'woman'con :
10,20,40,60,80,100 'woman'


4)womans_5places
///////////////////////////////////////////////////////////////
/Woman.cpl base con 5 atomi alla head (scelta tra 5 'places'):/
///////////////////////////////////////////////////////////////

woman(mary).
woman(andrea).
//etc....


like(Name,dance):0.6 ;like(Name,ballet):0.4:-woman(Name).
goesto(Name,pub):0.2;goesto(Name,disco):0.4;goesto(Name,restaurant):0.2;
goesto(Name,home):0.1;goesto(Name,park):0.1 :- woman(Name),like(Name,dance).
goesto(Name,pub):0.2;goesto(Name,theatre):0.4;goesto(Name,restaurant):0.2;
goesto(Name,home):0.1;goesto(Name,park):0.1 :- woman(Name),like(Name,ballet).


poi eseguio il test aumentando il numero di 'woman'con :
10,20,40,60,80,100 'woman'



///////////////////////////
QUERY:
goesto(mary,pub).
///////////////////////////


NB: i test saranno divisi per quanto riguarda le 4 tipologie di inferenza
mln
	- map
	- gibbs sampling
	- simulated tempering
	- MC-SAT
lpad
lpadsld
cpl


Test