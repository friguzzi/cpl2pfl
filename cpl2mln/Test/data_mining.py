#
# Script che raccoglie i dati dei test e forma un dizionario
# questo ci permette di manipolarli e rappresentarli come si desidera
#

import woman_data_generator, test_utility, os, re, csv


test_results = {}

def get_info_form_file(path, regExpResults, groupResult, regExpTime,  groupTime):
	'''legge il file e ritorna le informazioni in una tupla (nome_file,arg1,arg2,...,argn) per esempio 
	(nome_file,num,tempo)
	'''	
	result = None
	file_name = path
	time = None
	if(os.path.isfile(path)):
		try:
			file = open(path,'r')
			for line in file:
				#print line
				if(result == None):
					for regExpResult in regExpResults:					
						pResult = regExpResult.search(line)
						if(pResult != None and result == None):
							result = pResult.group(groupResult)	
				if(time == None):
					pTime = regExpTime.search(line)	
					if (pTime != None):
						time = pTime.group(groupTime)
			#print('  ' + str(result) + ',' + str(time))
			if(time != None and result != None):
				return (file_name,result,time)
			else:
				return None			
		finally:
			file.close()
	else:
		return None


def get_info():
	'''estrae le informazioni dai test eseguiti
	'''
	#struttura del direttorio da analizzare
	test_dir = woman_data_generator.create_woman_test_dir_names(test_utility.top_dir,test_utility.places, test_utility.places_dir_def, test_utility.womans, test_utility.womans_dir_def, test_utility.cplint_dir, test_utility.cplint_type, test_utility.alchemy_dir, test_utility.alchemy_type)		
	test_keys = woman_data_generator.create_woman_test_keys_tuples(test_utility.places, test_utility.womans, test_utility.cplint_dir, test_utility.cplint_type, test_utility.alchemy_dir, test_utility.alchemy_type)	
		
	#per ogni struttura
	for td, tk in zip(test_dir, test_keys):
		
		#aggiungo tk come chiave e tupla valore al dizionario		
		test_results[tk] = get_info_form_file(os.path.join(woman_data_generator.join_dir2path(td), test_utility.result_file_name), [re.compile("RESULT:\[(?P<num>.+)\]"),re.compile(".* (?P<num>.+)")], 'num', re.compile("TIME:\[(?P<num>.+)\]"), 'num')
		
	return test_results	


def write_cvs_test_result(file_path, dictionary, type):
	data = [] #lista da passare 	
	if (type == 1):
		#data.append('luoghi', 'donne', 'risolutore','algoritmo','risultato','tempo')
		for k, v in dictionary.iteritems():		
			if v == None:
				data.append([errore, v, k])
			else:
				L_comodo_key = list(k)
				L_comodo_value = list(v)
				L_comodo_value.pop(0)
				L_comodo_key.extend(L_comodo_value)
				data.append(L_comodo_key)
	elif(type == 2):
		for k, v in dictionary.iteritems():		
			if v == None:
				data.append([errore, v, k])
			else:				
				L_comodo_key = []
				L_comodo_key.append(k[0])
				L_comodo_key.append(k[2])
				L_comodo_key.append(k[3])
				L_comodo_key.append(k[1])
				L_comodo_value = list(v)
				L_comodo_value.pop(0)
				L_comodo_key.extend(L_comodo_value)
				data.append(L_comodo_key)
	else:
		print 'errore'
	data.sort()	
	writer = csv.writer(open(file_path, "wb"), delimiter=';')
	writer.writerows(data)

if __name__ == '__main__':
	print 'esecuzione stand-alone iniziata'	
	write_cvs_test_result(os.getcwd()+'/data_1.csv', get_info(), 1)
	write_cvs_test_result(os.getcwd()+'/data_2.csv', get_info(), 2)
	print 'esecuzione stand-alone terminata'

