#
# modulo contenente routine di utilità come quella relativa alla 
# ricerca dei file da eseguire in un albeero di directory
# 
import os, sys
from stat import *


#variabili globali
query_cplint = 's([goesto(w1,pub)],P)'  # 'sc([goesto(w1,disco)],[like(w1,dance)],P)'
query_alchemy = 'Goesto(W1,Pub)'	# 'Goesto(W1,Disco)'
alchemy_home = '/root/alchemy/bin/'
translator_home = '/root/Desktop/Redazione Tesi/Traduttore/src/cpl2mln_translator.pl'

top_dir = 'woman_test'
places = range(2,6)
places_dir_def = '_places'
womans = [10, 20, 40, 80, 100, 200, 500, 1000]
womans_dir_def = '_womans'
cplint_dir = 'cplint'
cplint_type = ['cpl_result', 'lpad_result', 'lpadsld_result']
alchemy_dir = 'alchemy'
alchemy_type = ['map_result', 'gibbs_result', 'simtp_result', 'mcsat_result']
cpl_file_name = 'woman.cpl'
uni_file_name = 'woman.uni'

pref_list = ['dance', 'ballet']
places_list = ['pub', 'disco', 'theatre', 'restaurant', 'home', 'park']
rules_txt = ["like(Name,dance):0.6;like(Name,ballet):0.4:-woman(Name).\ngoesto(Name,pub):0.4;goesto(Name,disco):0.6:-woman(Name),like(Name,dance).\ngoesto(Name,pub):0.4 ; goesto(Name,theatre):0.6 :- woman(Name),like(Name,ballet).\n", "like(Name,dance):0.6;like(Name,ballet):0.4:-woman(Name).\ngoesto(Name,pub):0.3;goesto(Name,disco):0.5;goesto(Name,restaurant):0.2:-woman(Name),like(Name,dance).\ngoesto(Name,pub):0.3;goesto(Name,theatre):0.5;goesto(Name,restaurant):0.2:-woman(Name),like(Name,ballet).\n", "like(Name,dance):0.6;like(Name,ballet):0.4:-woman(Name).\ngoesto(Name,pub):0.2;goesto(Name,disco):0.5;goesto(Name,restaurant):0.2;goesto(Name,home):0.1:-woman(Name),like(Name,dance).\ngoesto(Name,pub):0.2;goesto(Name,theatre):0.5;goesto(Name,restaurant):0.2;goesto(Name,home):0.1:-woman(Name),like(Name,ballet).", "like(Name,dance):0.6;like(Name,ballet):0.4:-woman(Name).\ngoesto(Name,pub):0.2;goesto(Name,disco):0.4;goesto(Name,restaurant):0.2;goesto(Name,home):0.1;goesto(Name,park):0.1:-woman(Name),like(Name,dance).\ngoesto(Name,pub):0.2;goesto(Name,theatre):0.4;goesto(Name,restaurant):0.2;goesto(Name,home):0.1;goesto(Name,park):0.1:-woman(Name),like(Name,ballet)."]

yap_translation_file_txt = "#!/usr/local/bin/yap -L --\n:- yap_flag(informational_messages,on),use_module('"+ translator_home +"'),set(grounding_generation,false),tr(woman, '../alchemy/woman')."

yap_translation_file_name = "translate_cpl2mln.pl"

def get_yap_execute_tests_cplint_file_lpadsld_txt():
	return "#!/usr/local/bin/yap -L -- \n:- open('./lpadsld_result/res.txt',write,S),current_output(O),set_output(S),yap_flag(informational_messages,on),use_module('/root/cplint_2/lpadsld.pl'),p(woman),write('QUERY:"+query_cplint+"'),nl,statistics(cputime,_),findall(P,"+query_cplint+",L),statistics(cputime,[_,T]),Tm is T / 1000,write('RESULT:'),write(L),nl,write('TIME:['),write(Tm), write('] sec'),nl,set_output(O),close(S)."

yap_execute_tests_cplint_file_lpadsld_name = "exe_cplint_lpadsld_test.pl"

def get_yap_execute_tests_cplint_file_lpad_txt():
	return "#!/usr/local/bin/yap -L -- \n:- open('./lpad_result/res.txt',write,S),current_output(O),set_output(S),yap_flag(informational_messages,on),use_module('/root/cplint_2/lpad.pl'),p(woman),write('QUERY:"+query_cplint+"'),nl,statistics(cputime,_),findall(P,"+query_cplint+",L),statistics(cputime,[_,T]),Tm is T / 1000,write('RESULT:'),write(L),nl,write('TIME:['),write(Tm), write('] sec'),nl,set_output(O),close(S)."

yap_execute_tests_cplint_file_lpad_name = "exe_cplint_lpad_test.pl"

def get_yap_execute_tests_cplint_file_cpl_txt():
	return "#!/usr/local/bin/yap -L -- \n:- open('./cpl_result/res.txt',write,S),current_output(O),set_output(S),yap_flag(informational_messages,on),use_module('/root/cplint_2/cpl.pl'),p(woman),write('QUERY:"+query_cplint+"'),nl,statistics(cputime,_),findall(P,"+query_cplint+",L),statistics(cputime,[_,T]),Tm is T / 1000,write('RESULT:'),write(L),nl,write('TIME:['),write(Tm), write('] sec'),nl,set_output(O),close(S)."

yap_execute_tests_cplint_file_cpl_name = "exe_cplint_cpl_test.pl"

alchemy_empty_db_file_txt = ""	# 'Like(W1,Dance)'

alchemy_empty_db_file_name = "empty.db"

execute_tests_alchemy_file_name = 'execute_tests_alchemy'

result_file_name = 'res.txt'

def get_execute_tests_alchemy_file_txt(path): 
	return "#!/bin/sh\n\n/usr/bin/time  -f 'TIME:[%U]' -o '" + path + "/gibbs_result/res.txt' --append '"+ alchemy_home +"infer' -i '" + path + "/woman.mln' -e '" + path + "/empty.db' -r '" + path + "/gibbs_result/res.txt' -q '"+query_alchemy+"' -p -numChains 20 -maxSteps 20000\n/usr/bin/time  -f 'TIME:[ %U]' -o '" + path + "/map_result/res.txt' --append '"+ alchemy_home +"infer' -i '" + path + "/woman.mln' -e '" + path + "/empty.db' -r '" + path + "/map_result/res.txt' -q '"+query_alchemy+"' -a \n/usr/bin/time  -f 'TIME:[%U]' -o '" + path + "/simtp_result/res.txt' --append '"+ alchemy_home +"infer' -i '" + path + "/woman.mln' -e '" + path + "/empty.db' -r '" + path + "/simtp_result/res.txt' -q '"+query_alchemy+"' -simtp -numRuns 5  -numSwap 20\n/usr/bin/time  -f 'TIME:[%U]' -o '" + path + "/mcsat_result/res.txt' --append '"+ alchemy_home +"infer' -i '" + path + "/woman.mln' -e '" + path + "/empty.db' -r '" + path + "/mcsat_result/res.txt' -q '"+query_alchemy+"' -ms -tries 10 -burnMaxSteps -1 -maxSteps 20000"



def write_file(path, file_name, txt):
	""" classica funzione per la scrittura su file
	"""
	file_path = os.path.join(path,file_name)
	try:
		f = open (file_path,'w')	
		f.write(txt)
	finally:
		f.close()
	os.chmod(file_path,777)

def walktree(top, filter, callback):
    '''recursively descend the directory tree rooted at top,
       calling the callback function for each regular file'''

    for f in os.listdir(top):
        pathname = os.path.join(top, f)
        mode = os.stat(pathname)[ST_MODE]
        if S_ISDIR(mode):
            # It's a directory, recurse into it
            walktree(pathname, filter, callback)
        elif S_ISREG(mode) and (filter(f)):
            # It's a file, call the callback function
            callback(pathname)
        else:
            # Unknown file type, print a message
            print 'Skipping %s' % pathname