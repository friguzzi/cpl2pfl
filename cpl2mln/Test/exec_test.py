#
# Script che esegue ricorsivamente su tutti i modelli i test alchemy e cplint
#

import os, test_utility

def filter_cplint(f):
	return (f == test_utility.yap_execute_tests_cplint_file_lpad_name) or (f == test_utility.yap_execute_tests_cplint_file_lpadsld_name) or (f == test_utility.yap_execute_tests_cplint_file_cpl_name)

def test_cplint(path_cmd):
	os.spawnl(os.P_WAIT, path_cmd)

def filter_alchemy(f):
	return (f == test_utility.execute_tests_alchemy_file_name)

def test_alchemy(path_cmd):
	print os.spawnl(os.P_WAIT, path_cmd)	

def exe_test(cmd):
	'''Funzione che esegue i test in base al tipo di comando
	cmd = 1 test su cplint
	cmd = 2 test su alchemy
	cmd = 3 test su cplint e alchemy
	'''
	if(cmd == 1):
		#test su cplint
		test_utility.walktree(os.getcwd(), filter_cplint, test_cplint)				
	elif(cmd == 2):
		#test su alchemy
		test_utility.walktree(os.getcwd(), filter_alchemy, test_alchemy)
	elif(cmd == 3):
		#test su cplint
		test_utility.walktree(os.getcwd(), filter_cplint, test_cplint)
		#test su alchemy
		test_utility.walktree(os.getcwd(), filter_alchemy, test_alchemy)
	else:
		print 'errore in esecuzione'

if __name__ == '__main__':
	print 'esecuzione stand-alone iniziata'
	cmd = None	
	while(cmd == None):
		print 'digitare 1 per esecuzione tests su suite Cplint'
		print 'digitare 2 per esecuzione tests su suite Alchemy'
		print 'digitare 3 per esecuzione tests su suite Cplint e Alchemy'			
		try:		
			cmd = int(raw_input("inserire il numero:"))
		except:
			print "input non valido.\n\n\n"			
			cmd = None
	exe_test(cmd) #esecuzione dei test
	print 'esecuzione stand-alone terminata'