/*
 * This file includes code from Chiarini Mattia
 * for input output operation on video and files
 * Author Mattia Chiarini
 */
:- module(cpl2mln_traslator_input_output, 
	[
	read_clauses_ground_body/2,
	read_clauses_exist_body/2,
	read_clauses_uni/2,
	read_clauses_cpl/2,
	create_translations_file/1,
	merge_translation_file/1,
	remove_translations_files/1
	]).


:-use_module(library(lists)).
:-use_module('./cpl2mln_translator',[setting/2,set/2]).
:-set_prolog_flag(single_var_warnings,on).
:-source.



/*
 * Remove translations file from Unix File System 
 */
remove_translations_files(FileName):-
	atom_concat(FileName,'.mln"',RulesFile),
	atom_concat('rm "',RulesFile,RulesFileDelCommand),
	unix(system(RulesFileDelCommand)),
	atom_concat(FileName,'_grounds.mln"',GroundsFile),
	atom_concat('rm "',GroundsFile,GroundsFileDelCommand),
	system(GroundsFileDelCommand).
	

merge_translation_file(FileName):-	
	atom_concat(FileName,'.mln',MlnFile),
	open(MlnFile,write,MlnFileS),
	atom_concat(FileName,'_grounds.mln',GroundsFile),
	open(GroundsFile,read,GroundsFileS),
	write_file2file(MlnFileS,GroundsFileS),	
	atom_concat(FileName,'_rules.mln',RulesFile),
	open(RulesFile,read,RuleFileS),
	write_file2file(MlnFileS,RuleFileS),
	close(GroundsFile),
	close(RuleFileS),
	close(MlnFileS).
	
write_file2file(_,InStream):-
	at_end_of_stream(InStream),!.
	
write_file2file(OutStream,InStream):-	
	get0(InStream,C),
	put(OutStream,C),
	write_file2file(OutStream,InStream).

/*
 * Create Translation Files
 */
create_translations_file(FileName):-
	atom_concat(FileName,'.mln',RulesFile),
	open(RulesFile,write,S),
	current_output(O),
	set_output(S),	
	set_output(O),
	close(S).

/*
 * read clauses universe file
 */
read_clauses_uni(S,[Cl|Out]):-
	read_term(S,Cl,[]),
	(Cl=end_of_file->
		Out=[]
	;
		read_clauses_uni(S,Out)
	).


/*
 * Read clauses in a 'cplint file' 
 */
read_clauses_cpl(S,Clauses):-
	(setting(ground_body,true)->
		read_clauses_ground_body(S,Clauses)
	;
		read_clauses_exist_body(S,Clauses)
	).


/*
 * type of Reading clauses cplint file
 */
read_clauses_ground_body(S,[(Cl,V)|Out]):-
	read_term(S,Cl,[variable_names(V)]),
	(Cl=end_of_file->
		Out=[]
	;
		read_clauses_ground_body(S,Out)
	).


read_clauses_exist_body(S,[(Cl,V)|Out]):-
	read_term(S,Cl,[variable_names(VN)]),
	extract_vars_cl(Cl,VN,V),
	(Cl=end_of_file->
		Out=[]
	;
		read_clauses_exist_body(S,Out)
	).

/*
 * Extract vars from a clause
 */
extract_vars_cl(end_of_file,[]).

extract_vars_cl(Cl,VN,Couples):-
	(Cl=(H:-_B)->
		true
	;
		H=Cl
	),
	extract_vars(H,[],V),
	pair(VN,V,Couples).
	
/*
 * Pair vars
 */
pair(_VN,[],[]).

pair([VN= _V|TVN],[V|TV],[VN=V|T]):-
	pair(TVN,TV,T).

extract_vars(Var,V0,V):-
	var(Var),!,
	(member_eq(Var,V0)->
		V=V0
	;
		append(V0,[Var],V)
	).
	
extract_vars(Term,V0,V):-
	Term=..[_F|Args],
	extract_vars_list(Args,V0,V).

extract_vars_list([],V,V).

extract_vars_list([Term|T],V0,V):-
	extract_vars(Term,V0,V1),
	extract_vars_list(T,V1,V).

/*
 * control if a list contains a predicate.
 */
member_eq(A,[H|_T]):-
	A==H.
	
member_eq(A,[_H|T]):-
	member_eq(A,T).