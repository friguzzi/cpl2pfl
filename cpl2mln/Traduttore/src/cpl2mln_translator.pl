/*
 cpl2mln_translator 	module
 Program that translate a 'cplint file' and an 'universe file' in a 'mln file'
 Author Mattia Chiarini
 */

:- module(cpl2mln_translator,[
	translate/2,
	tr/2,
	set/2,
	setting/2
	]).

:-use_module(library(lists)).
:-use_module('./cpl2mln_translator_input_output').		% input output operations
:-use_module('./cpl2mln_translator_uni_parser').		% translation 'universe file' 
:-use_module('./cpl2mln_translator_cpl_parser').		% translation 'cplint file'

:-dynamic setting/2.
:-set_prolog_flag(single_var_warnings,on).
:-source.



% Cplint default settings
setting(epsilon,0.00001).
setting(ground_body,true).		% [ita] se non ha true il funzionamento non è garantito
setting(test_builtins,false).	

% Translator settings
setting(grounding,modes). 		% grounding,[variables,modes]
setting(existential,false).		% [ita] d introdurre l'esistenziale per variabili che non sono 
					% instanziate nella testa e nemmeno presenti nella testa ma presenti nel body
setting(grounding_generation,false). 	%[ita] nella traduzione genera tutti i possibili ground di una clausola

/*
 Translate
 Argument: 
	FileName is the 'cplint file' and  'universe file' name
*/
tr(FileName,FileNameOut):-
	translate(FileName,FileNameOut).

translate(FileName,FileNameOut):-	

	%%%%%%%%%%%%%%%%%%
	% Startup system %
	%%%%%%%%%%%%%%%%%%

	% clean db
	retractall(root(_)),
	retractall(clauses(_)),
	retractall(herbrand_base(_)),
	retractall(node(_,_,_,_,_)),
	retractall(new_number(_)),
	assert(new_number(0)),		
	
	% create translation's files	
	create_translations_file(FileNameOut),

	%%%%%%%%%%%%%%%%%%%%%%%
	% Reading input files %
	%%%%%%%%%%%%%%%%%%%%%%%	

	% read clauses file *.uni
	atom_concat(FileName,'.uni',FileNameUni),
	
	% add to yap db FileNameUni for cpl2mln_translator_uni_parser
	make_reconsult_translator_uni(FileNameUni),

	% read clauses file *.cpl
	atom_concat(FileName,'.cpl',FileNameCpl),
	open(FileNameCpl,read,StreamCpl),
	read_clauses_cpl(StreamCpl,ClausesCpl),
	close(StreamCpl),
	%write('ClausesCpl: '),nl,write(ClausesCpl),nl,				% debug
	
	%%%%%%%%%%%%%%%%%%%%%%%
	% parsing input files %
	%%%%%%%%%%%%%%%%%%%%%%%

	% get ground's set from any predicate's argument
	choose_translate_uni(SetPredicatesUniOut,SetGroundsUniOut),
	%write('SetGroundsUniOut:'),nl,write(SetGroundsUniOut),nl,		% debug
	%write('SetPredicatesUniOut:'),nl,write(SetPredicatesUniOut),nl,	% debug

	% add to yap db FileNameUni for cpl2mln_translator_cpl_parser
	make_reconsult_translator_cpl(FileNameUni),	

	% get 'cplint file' parsing	
	process_clauses(ClausesCpl,ClausesCplVar),
	%write('ClausesCplVar:'),nl,write(ClausesCplVar),nl,			% debug	

	instantiate(ClausesCplVar,[],ClausesCplVarIst),
	%write('ClausesCplVarIst:'),nl,write(ClausesCplVarIst),nl,		% debug

	% get herbrand base
	build_herbrand_base(HB),
	%write('HB:'),nl,write(HB),nl,						% debug
	
	% build a memory entity: predicate list counter
	hidden_predicateList_counter(SetPredicatesUniOut,PredicateListCounter),
	%write('PredicateListCounter:'),nl,write(PredicateListCounter),nl,	% debug	

	% istantiate all clause
	choose_istantiate_clauses(ClausesCplVar,ClausesCplVarIst,ClausesCplVarChoosed),	
	%write('ClausesCplVarChoosed : '),nl,write(ClausesCplVarChoosed),nl,	% debug

	% translate the cplint clauses in mln clauses in 'Rule Definition'	
	translate_cpl(ClausesCplVarChoosed,HB,PredicateListCounter,OutPredicateListCounter,OutTranslateList),
	%write('PredicateListCounter after parsing:'),nl,write(OutPredicateListCounter),nl,	% debug
	%write('OutTranslateList after parsing:'),nl,write(OutTranslateList),nl,		% debug

	
	%%%%%%%%%%%%%%%%%%%%
	% Writing Mln file %
	%%%%%%%%%%%%%%%%%%%%	
	
	% write ground sets in 'Predicate Definition'
	write_cpl2mln_grounds(SetGroundsUniOut,FileNameOut),
	write('wrote variables domains...'),nl,

	% write real predicate to 'Predicate Definition'
	write_cpl2mln_predicates(SetPredicatesUniOut,FileNameOut),
	write('wrote  predicates...'),nl,
				
	% add hidden predicate to 'Predicate Definition'
	write_cpl2mln_hidden_predicates(SetPredicatesUniOut,FileNameOut,OutPredicateListCounter),
	write('wrote hidden predicates...'),nl,

	% write all rules translated in mln
	write_cpl2mln_translate(FileNameOut,OutTranslateList,HB),
	write('wrote mln rules...'),nl,

	% write hidden links to real predicates	in 'Rule Definition'
	write_hidden_notGround_links(SetPredicatesUniOut,FileNameOut,OutPredicateListCounter),
	write('write not grounds links...'),nl,

	% write negative-grounds predicates in 'Rule Definition'
	write_negative_grounds(ClausesCplVarIst,HB,FileNameOut),
	write('write negative grounds...'),nl,

	write('End tranlation...'),nl.


/*
 * Translate 'universe file' in grounds statements
 */
choose_translate_uni([],_):-
	setting(grounding, variables),!,
	write('Grounding choosed: variables'),nl,
	write('not yet implemented...'),nl.

choose_translate_uni(SetPredicatesUniOut ,SetGroundsUniOut):-
	setting(grounding,modes),!,
	write('Grounding choosed: modes'),nl,	
	read_modes_universe_term(SetPredicatesUniOut,SetGroundsUniOut).
		

choose_translate_uni(SetPredicatesUniOut,SetGroundsUniOut):-
	write("ERROR, setting(grounding,_) not valid."), nl,
	write("default value :modes..."),nl,
	set(grounding,modes),
	choose_translate_uni(SetPredicatesUniOut, SetGroundsUniOut).


/*
 * Select to translate clauses or grounded clauses.
 */
choose_istantiate_clauses(ClausesCplVar,_,ClausesCplVar):-
	setting(grounding_generation,false),!,
	write('Grounding_generation choosed: false'),nl,
	write('clauses are not grounded...'),nl.

choose_istantiate_clauses(_,ClausesCplVarIst,ClausesCplVarIstOut):-
	setting(grounding_generation,true),!,
	write('Grounding_generation choosed: true'),nl,	
	write('clauses are grounded...'),nl,
	clausesCplVarIst_trasform(ClausesCplVarIst,ClausesCplVarIstOut).

choose_istantiate_clauses(ClausesCplVar,_,ClausesCplVar):-
	write("ERROR, setting(grounding_generation,_) not valid."), nl,
	write("default value: false..."),nl,
	set(grounding_generation,false).

clausesCplVarIst_trasform([],[]).
clausesCplVarIst_trasform([r(OrList,ImplPredicates)|ClausesCplVarIst],[r([],OrList,ImplPredicates)|ClausesCplVarIstOut]):-
	clausesCplVarIst_trasform(ClausesCplVarIst,ClausesCplVarIstOut).

/*
 * create a list used to count 'builded hidden variables'
 * [(predicate_name,Counter)...]
 */
hidden_predicateList_counter([],[]).
hidden_predicateList_counter([HPredicate|TPredicates],[(PredicateName,0)|OutList]):-
	functor(HPredicate,PredicateName,_),
	hidden_predicateList_counter(TPredicates,OutList).

 
/* 
 * set(Par,Value) can be used to set the value of a parameter 
 */
set(ground_body,false):-
	write('Warning, setting(ground_body,false) may arrise exception if exist a clause body not ground.'), nl,
	retract(setting(ground_body,_)),
	assert(setting(ground_body,false)).

set(existential,true):-	
	setting(grounding_generation,true),!,
	write('Error setting(existential,true) may not be set if setting(grounding_generation,true)'), nl.

set(grounding_generation,true):-	
	setting(existential,true),!,
	write('Error setting(grounding_generation,true) may not be set if setting(existential,true)'), nl.

set(Parameter,Value):-	
	retract(setting(Parameter,_)),
	assert(setting(Parameter,Value)).
