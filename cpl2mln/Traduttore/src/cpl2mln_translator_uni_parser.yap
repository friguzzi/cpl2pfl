/*
 * Module used to read 'universe file'
 * Author Mattia Chiarini
 */
:- module(cpl2mln_translator_uni_parser,
	[
	make_reconsult_translator_uni/1,
	read_modes_universe_term/2,
	write_cpl2mln_predicates/2,
	write_cpl2mln_predicates/1,
	write_cpl2mln_predicate/4,
	write_cpl2mln_grounds/2,
	write_cpl2mln_grounds/1,
	write_cpl2mln_hidden_predicates/3
	]).

:-use_module(library(lists)).
:-set_prolog_flag(single_var_warnings,on).
:-source.

/*
 * read universe clauses and obtain 2 lists
 * 1st list contains predicates [predicate_name(args)]
 * 2nd list contains, variabile's groundings  [(variable_name,[grounding])]
 */

read_modes_universe_term(Predicates,Grounds):-
	findall(A,mode(A),Predicates),
	findall(([A],B),type(A,B),Grounds).

/*
 * Reconsult 'universe file'
 */
make_reconsult_translator_uni(FileNameUni):-
	reconsult(FileNameUni).

	
/*
 * write on a file all predicates in
 * MLN format
*/
write_cpl2mln_predicates(Predicates, FileName):-
	atom_concat(FileName,'.mln',File),
	open(File,append,S),
	current_output(O),
	set_output(S),
	nl,write('// Predicates...'),nl,		% write title
	write_cpl2mln_predicates(Predicates),
	set_output(O),
	close(S).

write_cpl2mln_predicates([]).

write_cpl2mln_predicates([Predicate|T]):-
	write_cpl2mln_predicate(Predicate,0,nohidden,defGround),
	format("~N",[]),
	write_cpl2mln_predicates(T).

write_cpl2mln_predicate(Predicate,Index,Hidden,DefGround):-
	functor(Predicate,PredicateName,Arity),
	uppercase_first_letter(PredicateName,UpperCaseFirstLetterPredicateName),
	create_hidden_predicate(UpperCaseFirstLetterPredicateName, Index,HiddenPredicateName),
	(
		Hidden == hidden ->
		format("~w(",[HiddenPredicateName])
		;
		format("~w(",[UpperCaseFirstLetterPredicateName])
	),
	write_cpl2mln_predicate_arg(Predicate,1,Arity,DefGround),
	format(")",[]).

write_cpl2mln_predicate_arg(Predicate,Arity,Arity,DefGround):-
	arg(Arity,Predicate,Arg),
	(
		DefGround == defGround ->
		format("~w_",[Arg])
		;
		format("~w",[Arg])
	).

write_cpl2mln_predicate_arg(Predicate,NArg,Arity,DefGround):-
	arg(NArg,Predicate,Arg),
	(
		DefGround == defGround ->
		format("~w_,",[Arg])
		;
		format("~w,",[Arg])
	),
	NArgn is NArg + 1,
	write_cpl2mln_predicate_arg(Predicate,NArgn,Arity,DefGround).

/*
 * create HIDDEN predicate in MLN format adding '_hiddenNumber' ending
 */
create_hidden_predicate(NamePredicate,Number,HiddenNamePredicate):-
	name(Number,AsciiNumber),		
	append([95],AsciiNumber,PostFixAscii),	
	name(PostFix,PostFixAscii),		
	atom_concat(NamePredicate,PostFix,HiddenNamePredicate).	


/*
 * write all ground sets on a file in 
 * MLN format
*/
write_cpl2mln_grounds(Gg, FileName):-
	atom_concat(FileName,'.mln',File),
	open(File,append,S),
	current_output(O),
	set_output(S),
	nl,write('// Ground sets...'),nl,		% write title
	write_cpl2mln_grounds(Gg),
	set_output(O),
	close(S).

write_cpl2mln_grounds([]).

write_cpl2mln_grounds([([Type],Elements)|T]):-	
	format("~w_ = {",[Type]),
	write_cpl2mln_grounds_element(Elements),
	format("}~N",[]),
	write_cpl2mln_grounds(T).

write_cpl2mln_grounds_element([H]):-
	uppercase_first_letter(H,UpperCaseH),
	format("~w",[UpperCaseH]).

write_cpl2mln_grounds_element([H|T]):-
	uppercase_first_letter(H,UpperCaseH),
	format("~w,",[UpperCaseH]),
	write_cpl2mln_grounds_element(T).



write_cpl2mln_hidden_predicates(SetPredicatesUniOut,RulesFileName,PredicateListCounter):-
	atom_concat(RulesFileName,'.mln',RulesFile),
	open(RulesFile,append,S), 							% open file to append
	current_output(O),								% save current standard output
	set_output(S),									% change current std output	
	nl,write('// Hidden predicates...'),nl,						% write title
	write_cpl2mln_hidden_predicates(SetPredicatesUniOut,PredicateListCounter),
	nl,
	set_output(O),									% recovery previus standard output
	close(S).

write_cpl2mln_hidden_predicates([],_).

write_cpl2mln_hidden_predicates([Predicate|Predicates],PredicateListCounter):-
	functor(Predicate,PredicateName,_),
	searchCounterValue(PredicateName,PredicateListCounter,Counter),
	write_cpl2mln_hidden_predicate(Predicate,1,Counter),
	write_cpl2mln_hidden_predicates(Predicates,PredicateListCounter).

/*
 * Search the counter value
 */
searchCounterValue(_,[],0).

searchCounterValue(PredicateName,[(PredicateName,Counter)|_],Counter).

searchCounterValue(PredicateName,[_|T],Counter):-
	searchCounterValue(PredicateName,T,Counter).

/*
 * Write not Grounds link
 */
write_cpl2mln_hidden_predicate(_,_,0).
write_cpl2mln_hidden_predicate(Predicate,MaxCounter,MaxCounter):-	
	write_cpl2mln_predicate(Predicate,MaxCounter,hidden,defGround),
	format("~N",[]).

write_cpl2mln_hidden_predicate(Predicate,Count,MaxCounter):-
	write_cpl2mln_predicate(Predicate,Count,hidden,defGround),
	format("~N",[]),
	Countn is Count + 1,
	write_cpl2mln_hidden_predicate(Predicate,Countn,MaxCounter).

/*
 * Utility, uppercase  first atom's letter
 */
uppercase_first_letter([],[]).

% if number return number
uppercase_first_letter(A, A):-
	number(A),!.

uppercase_first_letter(A, AUpper):-
	name(A,[H|T]),
	Hn is  H - 32,
	name(AUpper,[Hn|T]).