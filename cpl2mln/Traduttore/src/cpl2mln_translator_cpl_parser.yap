/*
 * This module help to parse a 'cplint file' and 
 * to generate mln rules
 * Author Mattia Chiarini
 */
:- module(cpl2mln_translator_cpl_parser,
	[
	process_clauses/2,
	make_reconsult_translator_cpl/1,
	instantiate/3,
	build_herbrand_base/1,
	translate_cpl/5,
	write_hidden_notGround_links/3,
	write_negative_grounds/3,
	write_cpl2mln_translate/3,
	write_cpl2mln_translate/2
	]).


:-use_module(library(lists)).
:-use_module('./cpl2mln_translator_uni_parser').		% translation 'universe file'
:-use_module('./cpl2mln_translator',[setting/2,set/2]).
:-set_prolog_flag(single_var_warnings,on).


/*
 * Reconsult 'universe file'
 */
make_reconsult_translator_cpl(FileNameUni):-
	reconsult(FileNameUni).


/* 
 * Predicates for processing the clauses read from the file 
 */
process_clauses([(end_of_file,[])],[]).

process_clauses([((H:-B),V)|T],[r(V,HL,B)|T1]):-
	H=(_;_),!,
	list2or(HL1,H),
	process_head(HL1,0,HL),
	process_clauses(T,T1).

process_clauses([((H:-B),V)|T],[r(V,HL,B)|T1]):-
	H=(_:_),!,
	list2or(HL1,H),
	process_head(HL1,0,HL),
	process_clauses(T,T1).
	
process_clauses([((H:-B),V)|T],[r(V,[H:1],B)|T1]):-!,
	process_clauses(T,T1).

process_clauses([(H,V)|T],[r(V,HL,true)|T1]):-
	H=(_;_),!,
	list2or(HL1,H),
	process_head(HL1,0,HL),
	process_clauses(T,T1).

process_clauses([(H,V)|T],[r(V,HL,true)|T1]):-
	H=(_:_),!,
	list2or(HL1,H),
	process_head(HL1,0,HL),
	process_clauses(T,T1).
	
process_clauses([(H,V)|T],[r(V,[H:1],true)|T1]):-
	process_clauses(T,T1).

process_head([H:PH],P,[H:PH1|Null]):-
	PH1 is PH,
	PNull is 1-P-PH1,
	setting(epsilon,Eps),
	EpsNeg is - Eps,
	PNull > EpsNeg,
	(PNull>Eps->
		Null=['':PNull]
	;
		Null=[]
	).

process_head([H:PH|T],P,[H:PH1|NT]):-
	PH1 is PH,
	P1 is P+PH1,
	process_head(T,P1,NT).



/*
 * Predicates for producing the ground instances of program clauses 
 */
instantiate([],C,C).

instantiate([r(_V,[H:1],true)|T],CIn,COut):-!,
	append(CIn,[r([H:1],true)],C1),
	instantiate(T,C1,COut).

instantiate([r(V,H,B)|T],CIn,COut):-	
	(setting(grounding,variables)->
		findall(r(H,BOut),instantiate_clause_variables(V,H,B,BOut),L)
	;
		findall(r(H,BOut),instantiate_clause_modes(H,B,BOut),L)
	),
	append(CIn,L,C1),
	instantiate(T,C1,COut).
		

instantiate_clause_modes(H,B,BOut):-
	instantiate_head_modes(H),
	list2and(BL,B),
	instantiate_body_modes(BL,BLOut),
	list2and(BLOut,BOut).


instantiate_head_modes([]):-!.

instantiate_head_modes([H:_P|T]):-
	instantiate_atom_modes(H),
	instantiate_head_modes(T).


instantiate_body_modes(BL,BL):-
	setting(ground_body,false),!.
	
instantiate_body_modes(BL0,BL):-
	instantiate_list_modes(BL0,BL).


instantiate_list_modes([],[]).

instantiate_list_modes([H|T0],T):-
	builtin(H),!,
	call(H),
	instantiate_list_modes(T0,T).

instantiate_list_modes([\+ H|T0],T):-
	builtin(H),!,
	\+ call(H),
	instantiate_list_modes(T0,T).

instantiate_list_modes([\+ H|T0],[\+ H|T]):-!,
	instantiate_atom_modes(H),
	instantiate_list_modes(T0,T).

instantiate_list_modes([H|T0],[H|T]):-
	instantiate_atom_modes(H),
	instantiate_list_modes(T0,T).


instantiate_atom_modes(''):-!.

instantiate_atom_modes(A):-
	functor(A,F,NArgs),
	functor(TA,F,NArgs),
	A=..[F|Args],
	mode(TA),
	TA=..[F|Types],
	instantiate_args_modes(Args,Types).


instantiate_args_modes([],[]):-!.

instantiate_args_modes([H|T],[TH|TT]):-
	type(TH,Constants),
	member(H,Constants),
	instantiate_args_modes(T,TT).


instantiate_clause_variables([],_H,B,BOut):-
	list2and(BL,B),
	(setting(ground_body,true)->
		check_body(BL,BLOut)
	;
		BLOut=BL
	),
	list2and(BLOut,BOut).

instantiate_clause_variables([VarName=Var|T],H,BIn,BOut):-
	universe(VarNames,U),
	member(VarName,VarNames),
	member(Var,U),
	instantiate_clause_variables(T,H,BIn,BOut).	

instantiate_clause_variables([VarName=_Var|T],H,BIn,BOut):-
	\+ varName_present_variables(VarName),!,
	instantiate_clause_variables(T,H,BIn,BOut).	


varName_present_variables(VarName):-
	universe(VarNames,_U), member(VarName,VarNames).


check_body([],[]).

check_body([H|T],TOut):-
	builtin(H),!,
	call(H),
	check_body(T,TOut).

check_body([H|T],[H|TOut]):-
	check_body(T,TOut).


/*
 * Predicate that return the herbrand base
 */
build_herbrand_base(HB):-
	findall(A,mode(A),LA),
	inst_list(LA,[],HB).

inst_list([],HB,HB):-!.

inst_list([H|T],HB0,HB):-
	%trace,
	functor(H,F,Args),
	functor(A,F,Args),
	findall(A,instantiate_atom(A),LA),	
	append(HB0,LA,HB1),
	inst_list(T,HB1,HB).

instantiate_atom(''):-!.

instantiate_atom(A):-
	functor(A,F,NArgs),
	functor(TA,F,NArgs),
	A=..[F|Args],
	mode(TA),
	TA=..[F|Types],
	instantiate_args(Args,Types).

instantiate_args([],[]):-!.

instantiate_args([H|T],[TH|TT]):-
	type(TH,Constants),
	member(H,Constants),
	instantiate_args(T,TT).

/*
 * Translate and write in a file cplint clauses 
 *
 * ClausesCplVar:clauses list,
 * HB: herbrandt base
 * InHiddenPredicateListCounter: inpuy list used to count 'builded hidden variables'
 * OutHiddenPredicateListCounter:output list used to count 'builded hidden variables'
 * OutTranslateList: output, contains a list o rules to parse in mln format the list is in this fomr
 */


translate_cpl([],_,PredicateListCounter,PredicateListCounter,[]).

% translate type A. rule (a simple fact)
translate_cpl([r(VarList,[(Predicate:1)],true)|T],HB,InPredicateListCounter,OutPredicateListCounter,
		[(r(VarList,[(Predicate:1)],true),[])|OutTranslateList]):-

	translate_cpl(T,HB,InPredicateListCounter,OutPredicateListCounter,OutTranslateList).

% translate type A:-B. rule
translate_cpl([r(VarList,[(Predicate:1)],ImplPredicates)|T],HB,InPredicateListCounter,OutPredicateListCounter,
		[(r(VarList,[(Predicate:1)],ImplPredicates),Index)|OutTranslateList]):-	
	functor(Predicate,PredicateName,_),
	%list2and(ImplPredicatesList,ImplPredicates),
	hidden_predicate_element_update(PredicateName,InPredicateListCounter,NPredicateListCounter,[],_,Index),
	translate_cpl(T,HB,NPredicateListCounter,OutPredicateListCounter,OutTranslateList).

% translate type A;B;C. rule
translate_cpl([r(VarList,OrList,true)|T],HB,InPredicateListCounter,OutPredicateListCounter,
		[(r(VarList,OrList,true),Indexes)|OutTranslateList]):-
	get_hidden_index(OrList,InPredicateListCounter,NPredicateListCounter,[],Indexes),%(in scrittura)
	translate_cpl(T,HB,NPredicateListCounter,OutPredicateListCounter,OutTranslateList).

% translate type A;B;C:-D,E. rule
translate_cpl([r(VarList,OrList,ImplPredicates)|T],HB,InPredicateListCounter,OutPredicateListCounter,
		[(r(VarList,OrList,ImplPredicates),Indexes)|OutTranslateList]):-
	%list2and(ImplPredicatesList,ImplPredicates),
	get_hidden_index(OrList,InPredicateListCounter,NPredicateListCounter,[],Indexes),
	translate_cpl(T,HB,NPredicateListCounter,OutPredicateListCounter,OutTranslateList).


/*
 * Write a predicate in MLN format
 */
% negative predicate
write_mln_predicate(\+Predicate,VarList,Truth):-
	functor(Predicate,PredicateName,Arity),
	uppercase_first_letter(PredicateName,UpperCaseFirstLetterPredicateName),
	(
		Truth == true ->
		format("!~w",[UpperCaseFirstLetterPredicateName])
		;
		format(" ~w",[UpperCaseFirstLetterPredicateName])
	),
	create_predicateMLN_arg(Predicate,1,Arity,VarList,ArgList),
	build_arg_set(ArgList,Args),
	format("(~w)",[Args]).

% positive predicate
write_mln_predicate(Predicate,VarList,Truth):-
	functor(Predicate,PredicateName,Arity),
	uppercase_first_letter(PredicateName,UpperCaseFirstLetterPredicateName),
	(
		Truth == true ->
		format(" ~w",[UpperCaseFirstLetterPredicateName])
		;
		format("!~w",[UpperCaseFirstLetterPredicateName])
	),
	create_predicateMLN_arg(Predicate,1,Arity,VarList,ArgList),
	build_arg_set(ArgList,Args),
	format("(~w)",[Args]).


/*
 * Write a HIDDEN predicate in MLN format
 */
% negative predicate
write_mln_predicate(\+Predicate,Index,VarList,Truth):-
	functor(Predicate,PredicateName,Arity),
	create_hidden_predicate(PredicateName, Index,HiddenPredicateName),
	uppercase_first_letter(HiddenPredicateName,UpperCaseFirstLetterPredicateName),
	(
		Truth == true ->
		format("!~w",[UpperCaseFirstLetterPredicateName])
		;
		format(" ~w",[UpperCaseFirstLetterPredicateName])
	),
	create_predicateMLN_arg(Predicate,1,Arity,VarList,ArgList),
	build_arg_set(ArgList,Args),
	format("(~w)",[Args]).

% positive predicate
write_mln_predicate(Predicate,Index,VarList,Truth):-
	functor(Predicate,PredicateName,Arity),
	create_hidden_predicate(PredicateName, Index,HiddenPredicateName),
	uppercase_first_letter(HiddenPredicateName,UpperCaseFirstLetterPredicateName),
	(
		Truth == true ->
		format(" ~w",[UpperCaseFirstLetterPredicateName])
		;
		format("!~w",[UpperCaseFirstLetterPredicateName])
	),
	create_predicateMLN_arg(Predicate,1,Arity,VarList,ArgList),
	build_arg_set(ArgList,Args),
	format("(~w)",[Args]).

/*
 * create HIDDEN predicate in MLN format adding '_hiddenNumber' ending
 */
create_hidden_predicate(NamePredicate,Number,HiddenNamePredicate):-
	name(Number,AsciiNumber),		
	append([95],AsciiNumber,PostFixAscii),	
	name(PostFix,PostFixAscii),		
	atom_concat(NamePredicate,PostFix,HiddenNamePredicate).	



/*
 * get a hidden index list for any predicates
 */

get_hidden_index([],InPredicateListCounter,InPredicateListCounter,_,[]):-!.% ultima modifica

get_hidden_index([(Predicate:_)|Tail],InPredicateListCounter,OutPredicateListCounter,VisitedPredicateName,[Index|List]):-
	functor(Predicate,PredicateName,_),	
	hidden_predicate_element_update(PredicateName,InPredicateListCounter,NPredicateListCounter,
					VisitedPredicateName,OutVisitedPredicateName,Index),
	get_hidden_index(Tail,NPredicateListCounter,OutPredicateListCounter,OutVisitedPredicateName,List).


/*
 * Update PredicateListCounter and get the current predicate's index
 */
% da utilizzare solo se non si vuole ripetizioni nella stessa clausola per predicati uguali
% hidden_predicate_element_update(PredicateName,[(PredicateName,N)|T],[(PredicateName,N)|T],
% 						VisitedPredicateName,VisitedPredicateName,N):-
% 	member(PredicateName,VisitedPredicateName),!. 

hidden_predicate_element_update(PredicateName,[(PredicateName,N)|T],[(PredicateName,Nn)|T],
						VisitedPredicateName,OutVisitedPredicateName,Nn):-
	append([PredicateName],VisitedPredicateName,OutVisitedPredicateName),
	Nn is N+1.

hidden_predicate_element_update(PredicateName,[H|T],[H|Out],VisitedPredicateName,OutVisitedPredicateName,Nn):-
	hidden_predicate_element_update(PredicateName,T,Out,VisitedPredicateName,OutVisitedPredicateName,Nn).



/*
 * Write MLN weighted rules
 */
write_mln_weighted_rules([],_,_,[],_).

write_mln_weighted_rules([H|T],AndList,VarList,[Ih|It],ExistVarList):-
	write_mln_weighted_rule(H,AndList,VarList,Ih,ExistVarList),
	write_mln_weighted_rules(T,AndList,VarList,It,ExistVarList).


/*
 * Write a MLN weighted rule
 */
write_mln_weighted_rule((Predicate:Weight),[],VarList,Index,_):-	
	number(Weight),!,
	N is log(Weight),
	format("~8g ",[N]),
	write_mln_predicate(Predicate,Index,VarList,true),
	format("~N",[]).

write_mln_weighted_rule((Predicate:Weight),AndList,VarList,Index,[]):-
	number(Weight),!,
	N is log(Weight),
	format("~8g ",[N]),	

	write_mln_predicate(Predicate,Index,VarList,true),
	format(" ^ ",[]),
	write_mln_andList(AndList,VarList),
	format("~N",[]).

write_mln_weighted_rule((Predicate:Weight),AndList,VarList,Index,ExistVarList):-	
	number(Weight),!,
	N is log(Weight),
	format("~8g ",[N]),
	%trace,							%debug 	
	write_mln_predicate(Predicate,Index,VarList,true),
	format(" ^ ",[]),
	write_mln_exist_variables(ExistVarList),		% write exist variables
	write_mln_andList(AndList,VarList),
	format(")~N",[]).	
	

/*
 * Write exixts variables
 */
write_mln_exist_variables(ExistVarList):-
	length(ExistVarList,Length),	
	(Length > 0 ->
		format(" EXIST ",[])
	;
		format(" ",[])
	),
	write_varList(ExistVarList), % write vars
	format(" (",[]).
	
/*
 * Find variable that are present only in the body
 */
get_mln_exist_variables([],VarList,VarList).
get_mln_exist_variables([(Predicate:_)|OrList],VarList,ExistVarsOut):-
	functor(Predicate,_,Arity),
	get_exist_variables_list(Predicate,1,Arity,VarList,ExistVars),
	get_mln_exist_variables(OrList,ExistVars,ExistVarsOut).

/*
 * Choose the variable that are present only in the body
 */
get_exist_variables_list(Predicate,Arity,Arity,VarList,ExistVars):-
	arg(Arity,Predicate,Arg),
	var(Arg),!,
	delete_var_varList(Arg,VarList,ExistVars).

get_exist_variables_list(_,Arity,Arity,VarList,VarList).

get_exist_variables_list(Predicate,N,Arity,VarList,ExistVars):-
	arg(N,Predicate,Arg),
	var(Arg),!,
	delete_var_varList(Arg,VarList,NVarList),
	Nn is N + 1,
	get_exist_variables_list(Predicate,Nn,Arity,NVarList,ExistVars).

get_exist_variables_list(Predicate,N,Arity,VarList,ExistVars):-
	Nn is N + 1,
	get_exist_variables_list(Predicate,Nn,Arity,VarList,ExistVars).


/*
 * Delete variable that are non included in the clauses'head form a VarList
 */	
delete_var_varList(_,[],[]).

delete_var_varList(Arg,[(_=Var)|T],Out):-		% found variable (delete)
	Arg == Var,!,
	delete_var_varList(Arg,T,Out).

delete_var_varList(Arg,[H|T],[H|Out]):-			% non found variable (no delete)
	delete_var_varList(Arg,T,Out).


/*
 * Write efectively a list of vars (comma separated)
 */
write_varList([]).

write_varList([(Name=_)]):-
	lowercase_first_letter(Name,LowerCaseFirstLetterName),
	format("~w",[LowerCaseFirstLetterName]).

write_varList([(Name=_)|T]):-
	lowercase_first_letter(Name,LowerCaseFirstLetterName),
	format("~w,",[LowerCaseFirstLetterName]),
	write_varList(T).


/*
 * Write MLN hard rules
 */
write_mln_hard_exor_rules([],OrList,[],VarList,[],Indexes,_,_):-
	write_mln_hard_exor_rule_allTrue(OrList,VarList,Indexes),
	format(".~N",[]).

write_mln_hard_exor_rules([],OrList,AndList,VarList,[],Indexes,ExistVarsOut,_):-
	length(ExistVarsOut,Length),
	write_mln_hard_exor_rule_allTrue(OrList,VarList,Indexes),
	format(" ~w ", ['<=>']),
	(Length > 0 ->
		format(" EXIST ",[])
	;
		format(" ",[])
	),
	(Length > 0 ->
		write_varList(ExistVarsOut) % write vars
	;
		format(" ",[])
	),
	(Length > 0 ->
		format(" (",[])
	;
		format(" ",[])
	),
	write_mln_andList(AndList,VarList),
	(Length > 0 ->
		format(" )",[])
	;
		format(" ",[])
	),
	format(".~N",[]).

write_mln_hard_exor_rules([H|T],OrList,AndList,VarList,[HIndex|TIndex],Indexes,ExistVarsOut,OldPairs):-
	write_mln_hard_exor_rule(H,OrList,VarList,HIndex,Indexes,OldPairs,NOldPairs),
	write_mln_hard_exor_rules(T,OrList,AndList,VarList,TIndex,Indexes,ExistVarsOut,NOldPairs).

write_mln_hard_exor_rule(_,[],_,_,_,OldPairs,OldPairs).

write_mln_hard_exor_rule((Predicate:_),[(OrPredicate:_)|OrList],VarList,Index,[_|Indexes],OldPairs,OldPairsOut):-
	already_wrote(Predicate,OrPredicate,OldPairs),!,
	write_mln_hard_exor_rule((Predicate:_),OrList,VarList,Index,Indexes,OldPairs,OldPairsOut).

write_mln_hard_exor_rule((Predicate:_),[(OrPredicate:_)|OrList],VarList,Index,[OrPredicateIndex|Indexes],OldPairs,OldPairsOut):-
	write_mln_predicate(Predicate,Index,VarList,false),
	format(" v ", []),
	write_mln_predicate(OrPredicate,OrPredicateIndex,VarList,false),
	format(".~N",[]),
	add_already_wrote(Predicate,OrPredicate,OldPairs,NOldPairs),
	write_mln_hard_exor_rule((Predicate:_),OrList,VarList,Index,Indexes,NOldPairs,OldPairsOut).

already_wrote(Predicate,OrPredicate,_):-
	(Predicate == OrPredicate).

already_wrote(Predicate,OrPredicate,OldPairs):-	
	member((Predicate,OrPredicate),OldPairs).

add_already_wrote(Predicate,OrPredicate,OldPairs,[(Predicate,OrPredicate),(OrPredicate,Predicate)|OldPairs]).
	

write_mln_hard_exor_rule_allTrue([(OrPredicate:_)],VarList,[OrPredicateIndex]):-
	write_mln_predicate(OrPredicate,OrPredicateIndex,VarList,true).

write_mln_hard_exor_rule_allTrue([(OrPredicate:_)|OrList],VarList,[OrPredicateIndex|Indexes]):-
	write_mln_predicate(OrPredicate,OrPredicateIndex,VarList,true),
	format(" v ", []),
	write_mln_hard_exor_rule_allTrue(OrList,VarList,Indexes).

/*
 * Write MLN hard rules
 */
write_mln_hard_exor_rules(OrList,[],0,VarList,Indexes,_):-
	write_mln_hard_exor_rule(OrList,0,VarList,Indexes),
	format(".~N",[]).

write_mln_hard_exor_rules(OrList,AndList,0,VarList,Indexes,ExistVarsOut):-
	length(ExistVarsOut,Length),	
	write_mln_hard_exor_rule(OrList,0,VarList,Indexes),
	format(" ~w ", ['<=>']),
	(Length > 0 ->
		format(" EXIST ",[])
	;
		format(" ",[])
	),
	(Length > 0 ->
		write_varList(ExistVarsOut) % write vars
	;
		format(" ",[])
	),
	(Length > 0 ->
		format(" (",[])
	;
		format(" ",[])
	),
	write_mln_andList(AndList,VarList),
	(Length > 0 ->
		format(" )",[])
	;
		format(" ",[])
	),
	format(".~N",[]).
	
write_mln_hard_exor_rules(OrList,AndList,Num,VarList,Indexes,ExistVarsOut):-
	check_2_pow(Num),!,
	Numn is Num - 1,
	write_mln_hard_exor_rules(OrList,AndList,Numn,VarList,Indexes,ExistVarsOut).

write_mln_hard_exor_rules(OrList,AndList,Num,VarList,Indexes,ExistVarsOut):-
	write_mln_hard_exor_rule(OrList,Num,VarList,Indexes),
	format(".~N",[]),
	Numn is Num - 1,
	write_mln_hard_exor_rules(OrList,AndList,Numn,VarList,Indexes,ExistVarsOut).


/*
 * Check if the number is power of nuber 2
 */
check_2_pow(X):-
	X > 0,!,
	check_2_pow(X,0),!.

check_2_pow(X,Exp):-
	Value is integer(2 ^ Exp),	
	X > Value,!,
	Expn is Exp + 1,
	check_2_pow(X,Expn).

check_2_pow(X,Exp):-
	Value is integer(2^Exp),!,
	X == Value.	


/*
 * Write a MLN hard rule
 */
write_mln_hard_exor_rule([(Predicate:_)],Num,VarList,[Index]):-
	Sign is Num mod 2 ,
	Sign == 1,!,
	% nego il predicato	
	write_mln_predicate(Predicate,Index,VarList,false).

write_mln_hard_exor_rule([(Predicate:_)],Num,VarList,[Index]):-
	Sign is Num mod 2 ,
	Sign == 0,!,
	% riporto il predicato	
	write_mln_predicate(Predicate,Index,VarList,true).

write_mln_hard_exor_rule([(Predicate:_)|T],Num,VarList,[Index|Indexes]):-
	Sign is Num mod 2 ,
	Sign == 1,!,
	% nego il predicato	
	write_mln_predicate(Predicate,Index,VarList,false),
	Numn is Num // 2,
	format(" v ",[]),
	write_mln_hard_exor_rule(T,Numn,VarList,Indexes).
	

write_mln_hard_exor_rule([(Predicate:_)|T],Num,VarList,[Index|Indexes]):-
	Sign is Num mod 2 ,
	Sign == 0,!,
	% riporto il predicato	
	write_mln_predicate(Predicate,Index,VarList,true),
	Numn is Num // 2,
	format(" v ",[]),
	write_mln_hard_exor_rule(T,Numn,VarList,Indexes).

/*
 * Write a list of AND predicates 
 */
write_mln_andList([Predicate],VarList):-
	write_mln_predicate(Predicate,VarList,true).

write_mln_andList([Predicate|T],VarList):-
	%trace,	%debug
	write_mln_predicate(Predicate,VarList,true),
	format(" ^ ",[]),
	write_mln_andList(T,VarList).


/*
 * Extract a list of arguments of form a predicate 
 * Par:
 * P: Predicate
 * PredicateName: Predicate's name
 * Number selected argument
 * Arguments arity
 * V: List variable's names 
 * Argument in MLN format
 */
create_predicateMLN_arg(P,Arity,Arity,V,[NVarName]):-	
	arg(Arity,P,Arg),
	var(Arg),!,
	get_var_name(Arg,V,VarName),	% find variable's name in V	
	lowercase_first_letter(VarName,NVarName).
	
create_predicateMLN_arg(P,Arity,Arity,_,[OutGround]):-
	arg(Arity,P,Ground),!,
	uppercase_first_letter(Ground,NGround),
	to_char_if_number(NGround,OutGround).	% trasform a number in a char

create_predicateMLN_arg(P,Num,Arity,V,[NVarName|ArgsList]):-	
	arg(Num,P,Arg),
	var(Arg),!,
	get_var_name(Arg,V,VarName),	% find variable's name in V	
	lowercase_first_letter(VarName,NVarName),
	Numn is Num + 1,
	create_predicateMLN_arg(P,Numn,Arity,V,ArgsList).

create_predicateMLN_arg(P,Num,Arity,V,[OutGround|ArgsList]):-	
	arg(Num,P,Ground),!,	
	uppercase_first_letter(Ground,NGround),	
	to_char_if_number(NGround,OutGround),	% trasform a number in a char
	Numn is Num + 1,
	create_predicateMLN_arg(P,Numn,Arity,V,ArgsList).


/*
 * Build string that contains all arguments in mln format
 */
build_arg_set([E],E).
build_arg_set([H|T],ArgSet):-
	build_arg_set(T,E),	
	atom_concat([H, ',',E],ArgSet).	

/*
 * If the input atom is a number, trasform in a string
 */
to_char_if_number(N,CharN):-
	number(N),!,
	number_atom(N,CharN).

to_char_if_number(N,N).


/*
 * Find variable's name in a list containing elements (type of [NAMEVAR=$Internal_symbol])
 */
get_var_name(_,[],_):-
	fail,!.

get_var_name(Arg,[(VarName=Var)|_],VarName):-
	Arg == Var,!.

get_var_name(Arg,[_|Tail],VarName):-
	get_var_name(Arg,Tail,VarName).

/*
 * Select a sub-set from herbreandt base where predicate name match
 * with a list of predicate names.
 */
% selectHB_predicateNames([],_,[],_,_).
% 
% selectHB_predicateNames([(Predicate:_)|T],PredicatesIst,[Index|Indexes],VarList,HB):-	
% 	selectHB_predicateName(Predicate,HB,NHB,PredicateHB),
% 	get_not_ground_predicates(PredicatesIst,PredicateHB,NotHB),	
% 	write_mln_not_ground_predicate(NotHB,VarList,Index),
% 	selectHB_predicateNames(T,PredicatesIst,Indexes,VarList,HB).
% 

/*
 * Write not Grounds links
 */

write_hidden_notGround_links(SetPredicatesUniOut,RulesFileName,PredicateListCounter):-
	atom_concat(RulesFileName,'.mln',RulesFile),
	open(RulesFile,append,S), 							% open file to append
	current_output(O),								% save current standard output
	set_output(S),									% change current std output	
	write('// Hidden predicates to real predicates...'),nl,				% write title
	write_hidden_notGround_links(SetPredicatesUniOut,PredicateListCounter),
	set_output(O),									% recovery previus standard output
	close(S).

write_hidden_notGround_links([],_).

write_hidden_notGround_links([Predicate|Predicates],PredicateListCounter):-
	functor(Predicate,PredicateName,_),
	searchCounterValue(PredicateName,PredicateListCounter,Counter),
	write_hidden_notGround_link(Predicate,1,Counter),
	write_hidden_notGround_links(Predicates,PredicateListCounter).

/*
 * Search the counter value
 */
searchCounterValue(_,[],0).

searchCounterValue(PredicateName,[(PredicateName,Counter)|_],Counter).

searchCounterValue(PredicateName,[_|T],Counter):-
	searchCounterValue(PredicateName,T,Counter).

/*
 * Write not Grounds link
 */
write_hidden_notGround_link(_,_,0).
write_hidden_notGround_link(Predicate,MaxCounter,MaxCounter):-
	write_cpl2mln_predicate(Predicate,MaxCounter,hidden,no_defGrounds),
	format(" <=> ",[]),
	write_cpl2mln_predicate(Predicate,MaxCounter,nohidden,no_defGrounds),
	format(".~N",[]).

write_hidden_notGround_link(Predicate,Count,MaxCounter):-
	write_cpl2mln_predicate(Predicate,Count,hidden,no_defGrounds),
	format(" v ",[]),
	Countn is Count + 1,
	write_hidden_notGround_link(Predicate,Countn,MaxCounter).

/*
 * write negative-grounds predicates	
 */
write_negative_grounds(ClausesCplVarIst,HB,FileName):-
	atom_concat(FileName,'.mln',RulesFile),
	open(RulesFile,append,S), 							% open file to append
	current_output(O),								% save current standard output
	set_output(S),									% change current std output	
	nl,write('// negative-grounds predicates...'),nl,					% write title	
	select_negative_grounds(ClausesCplVarIst,HB,OutHB),	
	write_list_negative_grounds(OutHB),
	set_output(O),									% recovery previus standard output
	close(S).

select_negative_grounds([],HB,HB).

select_negative_grounds([r(Predicates,_)|T],HB,OutHB):-
	select_negative_ground(Predicates,HB,NHB),
	select_negative_grounds(T,NHB,OutHB).

select_negative_ground([],HB,HB).

select_negative_ground([(Predicate:_)|Predicates],HB,OutHB):-
	delete(HB,Predicate,DHB),
	select_negative_ground(Predicates,DHB,OutHB).

write_list_negative_grounds([]).

write_list_negative_grounds([Predicate|Predicates]):-
	write_mln_predicate(Predicate,[],false),% write ground predicates
	format(".~N",[]),
	write_list_negative_grounds(Predicates).

	
/*
 * Write all rules translated in mln format
 * Parameters:
 * RulesFileName: mln file name
 * RulesList: set of rule to write in mln format
 * HB: herbrandt base
 */
write_cpl2mln_translate(RulesFileName,RuleList,HB):-
	atom_concat(RulesFileName,'.mln',RulesFile),
	open(RulesFile,append,S), 							% open file
	current_output(O),								% save current standard output
	set_output(S),									% change current standard output
	write_cpl2mln_translate(RuleList,HB),
	set_output(O),									% recovery previus standard output
	close(S).


/*
 * write a rule in mln format
 */
write_cpl2mln_translate([],_).

% write type A. rule (write a simple fact)
write_cpl2mln_translate([(r(VarList,[(Predicate:1)],true),_)|T],HB):-
	format("//fact ~N", []),
	write_mln_predicate(Predicate,VarList,true),
	format(".~2N",[]),
	write_cpl2mln_translate(T,HB).

% write type A:-B. rule
write_cpl2mln_translate([(r(VarList,[(Predicate:1)],ImplPredicates),Index)|T],HB):-	
	format("//rule type A:-B ~N", []),
	get_mln_exist_variables([(Predicate:1)],VarList,ExistVarsOut),
	
	(setting(existential,true)->
		length(ExistVarsOut,Length)
	;
		Length is 0
	),
		
	write_mln_predicate(Predicate,Index,VarList,true),
	format(" ~w ", ['<=>']),
	(Length > 0 ->
		format(" EXIST ",[])
	;
		format(" ",[])
	),
	
	(Length > 0 ->
		write_varList(ExistVarsOut) % write vars
	;
		format(" ",[])
	),

	(Length > 0 ->
		format(" (",[])
	;
		format(" ",[])
	),
	list2and(ImplPredicatesList,ImplPredicates),
	write_mln_andList(ImplPredicatesList,VarList),
	(Length > 0 ->
		format(" ).~N",[])
	;
		format(".~N",[])
	),
	
	% check grounding predicates not used in the head
	write('// not hidden:'),nl,	%(in scrittura)
	create_not_hidden_rules(r(VarList,[(Predicate:1)],ImplPredicates),[Index],HB,NotHidden),
	write_mln_not_ground_predicates(NotHidden,VarList),	
	format("~2N",[]),	
	write_cpl2mln_translate(T,HB).

% write type A;B;C. rule
write_cpl2mln_translate([(r(VarList,OrList,true),Indexes)|T],HB):-
	format("//rule type A;B;... ~N", []),	
	write_mln_weighted_rules(OrList,[],VarList,Indexes),
	%length(OrList,LenghtOrList),
	%Num is integer(((LenghtOrList)^2)-1),
	%write_mln_hard_exor_rules(OrList,[],Num,VarList,Indexes,[]),
	%trace,
	write_mln_hard_exor_rules(OrList,OrList,[],VarList,Indexes,Indexes,[],[]),
	% check grounding predicates not used in the head	
	write('// not hidden:'),nl,
	create_not_hidden_rules(r(VarList,OrList,true),Indexes,HB,NotHidden),
	write_mln_not_ground_predicates(NotHidden,VarList),
	format("~2N",[]),
	write_cpl2mln_translate(T,HB).

% write type A;B;C:-D,E. rule
write_cpl2mln_translate([(r(VarList,OrList,ImplPredicates),Indexes)|T],HB):-
	format("//rule type A;B;C... :-D,E... ~N", []),
	list2and(ImplPredicatesList,ImplPredicates),	
	get_mln_exist_variables(OrList,VarList,ExistVarsOut),	
	(setting(existential,true)->
		write_mln_weighted_rules(OrList,ImplPredicatesList,VarList,Indexes,ExistVarsOut)
	;
		write_mln_weighted_rules(OrList,ImplPredicatesList,VarList,Indexes,[])
	),
	%length(OrList,LenghtOrList),
	%Num is integer(((LenghtOrList)^2)-1),
	%write_mln_hard_exor_rules(OrList,ImplPredicatesList,Num,VarList,Indexes,ExistVarsOut),
	%trace,
	write_mln_hard_exor_rules(OrList,OrList,ImplPredicatesList,VarList,Indexes,Indexes,ExistVarsOut,[]),

	% check grounding predicates not used in the head	
	write('// not hidden:'),nl,
	create_not_hidden_rules(r(VarList,OrList,ImplPredicates),Indexes,HB,NotHidden),
	write_mln_not_ground_predicates(NotHidden,VarList),
 	format("~2N",[]),
 	write_cpl2mln_translate(T,HB).


/*
 * Create not hidden rules
 */
create_not_hidden_rules(r(_,[],_),[],_,[]).
create_not_hidden_rules(r(VarList,[(Predicate:Prob)|OrList],ImplPredicates),[Index|Indexes],HB,[r(NotHB,Index)|NotHidden]):-
	instantiate([r(VarList,[(Predicate:Prob)],ImplPredicates)],[],PredicatesIst),
	selectHB_predicateName(Predicate,HB,_,PredicateHB),
	get_not_ground_predicates(PredicatesIst,PredicateHB,NotHB),
	create_not_hidden_rules(r(VarList,OrList,ImplPredicates), Indexes, HB, NotHidden).

/*
 * Choose a set of predicates in herbrandt base like a Predicate
 */
selectHB_predicateName(_,[],[],[]).

selectHB_predicateName(\+Predicate,[H|T],NList,[H|AppendList]):-	% for negation
	functor(Predicate,PredicateName,Arity),
	functor(H,PredicateName,Arity),!,	% check 'H' and 'Predicate' are the same type of predicate
	selectHB_predicateName(Predicate,T,NList,AppendList).

selectHB_predicateName(Predicate,[H|T],NList,[H|AppendList]):-
	functor(Predicate,PredicateName,Arity),
	functor(H,PredicateName,Arity),!,	% check 'H' and 'Predicate' are the same type of predicate
	selectHB_predicateName(Predicate,T,NList,AppendList).

selectHB_predicateName(Predicate,[H|T],[H|NList],AppendList):-
	selectHB_predicateName(Predicate,T,NList,AppendList).


/*
 * Return a list where ground predicate are not present
 * in a list of grounding form istantiated clause
 */
get_not_ground_predicates([],HB,HB).
get_not_ground_predicates([r(Predicates,_)|T],HB,OutHB):-
	get_not_ground_predicate(Predicates,HB,NHB),
	get_not_ground_predicates(T,NHB,OutHB).

get_not_ground_predicate([],HB,HB).
get_not_ground_predicate([(Predicate:_)|T],HB,OutHB):-
	delete(HB,Predicate,NHB),!,
	get_not_ground_predicate(T,NHB,OutHB).
get_not_ground_predicate([_|T],HB,OutHB):-	%attention
	get_not_ground_predicate(T,HB,OutHB).


/*
 * Write rules declaring not ground hidden predicates in MLN format 
 */
write_mln_not_ground_predicates([],_).
write_mln_not_ground_predicates([r(GroundList,Index)|T],VarList):-
	write_mln_not_ground_predicate(GroundList,VarList,Index),
	write_mln_not_ground_predicates(T,VarList).

write_mln_not_ground_predicate([],_,_).
write_mln_not_ground_predicate([Predicate|T],VarList,Index):-
	write_mln_predicate(Predicate,Index,VarList,false),
	format(".~N",[]),
	write_mln_not_ground_predicate(T,VarList,Index).
	


/*
 * Utility, uppercase  first atom's letter
 */
uppercase_first_letter([],[]).

% if number return number
uppercase_first_letter(A, A):-
	number(A),!.

uppercase_first_letter(A, AUpper):-
	name(A,[H|T]),
	Hn is  H - 32,
	name(AUpper,[Hn|T]).


/*
 * Utility, lowercase  first atom's letter
 */
lowercase_first_letter([],[]).

% if a number return number
lowercase_first_letter(A, A):-
	number(A),!.

lowercase_first_letter(A, ALower):-
	name(A,[H|T]),
	Hn is  H + 32,
	name(ALower,[Hn|T]).



/* 
 * Auxiliary predicates 
 */
list2or([X],X):-
		X\=;(_,_),!.

list2or([H|T],(H ; Ta)):-!,
		list2or(T,Ta).


list2and([],true):-!.

list2and([X],X):-
		X\=(_,_),!.

list2and([H|T],(H,Ta)):-!,
		list2and(T,Ta).


builtin(_A is _B).
builtin(_A > _B).
builtin(_A < _B).
builtin(_A >= _B).
builtin(_A =< _B).
builtin(_A =:= _B).
builtin(_A =\= _B).
builtin(true).
builtin(false).
builtin(_A = _B).
builtin(_A==_B).
builtin(_A\=_B).
builtin(_A\==_B).


bg(member(_El,_L)).
bg(average(_L,_Av)).
bg(max_list(_L,_Max)).
bg(min_list(_L,_Max)).


average(L,Av):-
	sum_list(L,Sum),
	length(L,N),
	Av is Sum/N.
