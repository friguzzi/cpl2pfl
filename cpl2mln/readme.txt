Questa è la struttura di tutti i file che compongono  la tesi.
mi dica se le va bene?

I capitoli che ho riveduto sono:
Il capitolo 5 è stato modificato nel paragrafo 5.2
il capitolo 6 è stato completato e modificato interamente
il capitolo 7 è stato creato completamente
mi dica se sono corretti?

Perquanto riguardai test, le query sono le seguenti
1) query non condizionale
in lpad, cpl, lpadsld -> s([goesto(w1,pub)], P).
in gibbs sampling -> maxSteps 20000
in simulated tempering -> -numRuns 5  -numSwap 20
in MC-SAT tries 10 -burnMaxSteps -1 -maxSteps 20000

2) query condizionale (vorrei inserirla)
in lpad, cpl, lpadsld ->  sc([goesto(w1,disco)],[like(w1,dance)], P).
in gibbs sampling  opzione -> maxSteps 20000
in simulated tempering -> -numRuns 5  -numSwap 20
in MC-SAT -> tries 10  -burnMaxSteps -1 -maxSteps 20000
l'evidenza la inserisco nel file empty.db
(mettendo il fatto Like(W1,Dance) in empty.db  ottengo un risultato corretto)

I settaggi che riguardano gli algoritmi di alchemy sono modificati così:
Per i primi aumentiamo il numero degli step per far diventare sufficientemente lunga la Markov chain e avere una stima il pi\`u corretta possibile.
In MC-SAT dato che l'ergodicità viene rispettata comunque allora aumenriamo il numero di ripetizioni dell'algoritmo stesso.
Mi faccia sapere se le opzioni che ho scelto per ogni algoritmo di alchemy sono corrette anche per lei?
