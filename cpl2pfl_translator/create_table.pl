/*
 create_table
 Library for creation of the table of a parfactor.
 Author Zese Riccardo
 */
 
:- use_module(library(lists)).

/*
 * Creates table for OR parfactor
 */
build_or_table(Terms,P,T):-
	create_table(P,Terms,T,'OR').

/*
 * Creates table for AND parfactor
 */	
build_and_table(Terms,P,T):-
	create_table(P,Terms,T,'AND').

/*
 * Create table for general parfactor
 */
create_table(P,Terms,[NP,P],_):-
	length(Terms,1),!,
	NP is 1 - P.
	
create_table(P,[\+_|L],Table,Mode):-
	!,
	length(L,Length),
	NC is 2^(Length),
	compute_column_value(L,Val),
	NP is 1 - P,
	build_row(NC,Val,[P,NP],Row1,Mode),
	build_row(NC,Val,[NP,P],Row2,Mode),
	append(Row1,Row2,Table),
	!.
	
create_table(P,[_H|L],Table,Mode):-
	length(L,Length),
	NC is 2^(Length),
	compute_column_value(L,Val),
	NP is 1.0 - P,
	build_row(NC,Val,[NP,P],Row1,Mode),
	build_row(NC,Val,[P,NP],Row2,Mode),
	append(Row1,Row2,Table),
	!.
	
/*
 * Create a row for the table
 */
build_row(0,_,_,[],_):-!.

build_row(NC,Val,[P,NP],[Prob|Valore1],Mode):-
	compute_value(Val,Valore,Mode),
	(Valore == 't' -> Prob = P ; Prob = NP),
	next_column(Val,Val1),
	N is NC - 1,
	build_row(N,Val1,[P,NP],Valore1,Mode).
	
/*
 * Computes the truth value of a column label
 */	
compute_value(['t'],'t',_).
compute_value(['f'],'f',_).
compute_value([H|T],Val,Mode):-
	compute_value(T,V,Mode),
	(Mode == 'AND' -> and(H,V,Val) ; or(H,V,Val)).
	
/*
 * compute AND and OR
 */	
and('t','t','t').
and('f','t','f').
and('t','f','f').
and('f','f','f').

or('t','t','t').
or('f','t','t').
or('t','f','t').
or('f','f','f').

/*
 * Compute the next column by updating true/false value of the column label
 */	
next_column(Val,Val1):-
	next_column(Val,Val1,_).

next_column(['t'],['f'],1).

next_column(['f'],['t'],1).

next_column(['t'|T],[V1,V|T1],C1):-
	next_column(T,[V|T1],C),
	(C==1 ->
		( V=='t' -> 
			V1='t', C1=0 
	 	  ; 
	 		V1='f', C1=1)
	 ;
	 	V1='t', C1=0
	 ).

next_column(['f'|T],[V1,V|T1],C1):-
	next_column(T,[V|T1],C),
	(C==1 ->
		( V=='t' -> 
			V1='f', C1=0 
	 	  ; 
	 		V1='t', C1=1)
	 ;
	 	V1='f', C1=0
	 ).
	
	
	
/*
 * Initialize the label of the forst column
 */	
compute_column_value([],[]).

compute_column_value([\+_|T],['t'|T1]):-
	!,
	compute_column_value(T,T1).
	
compute_column_value([_|T],['f'|T1]):-
	compute_column_value(T,T1).
