:-table person(_h4811,myor / 3 - myzero / 1).
:-table attends(_h4860,myor / 3 - myzero / 1).
:-table sa(_h4909,myor / 3 - myzero / 1).
:-table series(myor / 3 - myzero / 1).
:-table reg(_h5004,_h5005,myor / 3 - myzero / 1).
:-table hot(_h5056,myor / 3 - myzero / 1).
:-table ah(_h5105,_h5106,myor / 3 - myzero / 1).
:-table workshop(_h5157,myor / 3 - myzero / 1).

(series(_h2112) :- myone(_h2039)  ','  person(_h245,_h2040)  ','  myand(_h2039,_h2040,_h2041)  ','  attends(_h245,_h2083)  ','  myand(_h2041,_h2083,_h2084)  ','  sa(_h245,_h2111)  ','  myand(_h2084,_h2111,_h2112)).
(sa(_h366,_h2344) :- (myone(_h2229)  ','  person(_h366,_h2230)  ','  myand(_h2229,_h2230,_h2231))  ','  get_var_n(1,[_h366],[0.5010,0.4990],_h2360)  ','  myequality(_h2360,0,_h2357)  ','  myand(_h2231,_h2357,_h2344)).
(attends(_h479,_h2473) :- myone(_h2410)  ','  reg(_h479,_h517,_h2411)  ','  myand(_h2410,_h2411,_h2412)  ','  hot(_h517,_h2444)  ','  myand(_h2412,_h2444,_h2445)  ','  ah(_h479,_h517,_h2472)  ','  myand(_h2445,_h2472,_h2473)).
(ah(_h660,_h674,_h2731) :- (myone(_h2601)  ','  reg(_h660,_h674,_h2602)  ','  myand(_h2601,_h2602,_h2603))  ','  get_var_n(2,[_h660,_h674],[0.8000,0.2000],_h2750)  ','  myequality(_h2750,0,_h2747)  ','  myand(_h2603,_h2747,_h2731)).
(hot(_h805,_h2949) :- (myone(_h2831)  ','  workshop(_h805,_h2832)  ','  myand(_h2831,_h2832,_h2833))  ','  get_var_n(3,[_h805],[0.5000,0.5000],_h2965)  ','  myequality(_h2965,0,_h2962)  ','  myand(_h2833,_h2962,_h2949)).
(person(p1,_h2999) :- myone(_h2999)).
(person(p2,_h3049) :- myone(_h3049)).
(person(p3,_h3101) :- myone(_h3101)).
(person(p4,_h3155) :- myone(_h3155)).
(person(p5,_h3211) :- myone(_h3211)).
(workshop(w1,_h3269) :- myone(_h3269)).
(workshop(w2,_h3329) :- myone(_h3329)).
(workshop(w3,_h3391) :- myone(_h3391)).
(reg(p1,w1,_h3455) :- myone(_h3455)).
(reg(p1,w2,_h3526) :- myone(_h3526)).
(reg(p1,w3,_h3599) :- myone(_h3599)).
(reg(p2,w1,_h3674) :- myone(_h3674)).
(reg(p2,w2,_h3751) :- myone(_h3751)).
(reg(p2,w3,_h3830) :- myone(_h3830)).
(reg(p3,w1,_h3911) :- myone(_h3911)).
(reg(p3,w2,_h3994) :- myone(_h3994)).
(reg(p3,w3,_h4079) :- myone(_h4079)).
(reg(p4,w1,_h4166) :- myone(_h4166)).
(reg(p4,w2,_h4255) :- myone(_h4255)).
(reg(p4,w3,_h4346) :- myone(_h4346)).
(reg(p5,w1,_h4439) :- myone(_h4439)).
(reg(p5,w2,_h4534) :- myone(_h4534)).
(reg(p5,w3,_h4631) :- myone(_h4631)).
