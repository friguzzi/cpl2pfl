
:- use_module(library(clpbn)).
:-use_module(library(lists)).
%:- set_solver(ve).
%p(1,X):0.1:-a(X).

choice(1,X,C):-
	a(X),
	{ C=c(1,X) with p([p(1,X),''],[0.1,0.9])}.


%p(X,2):0.2:-b(X).

choice(2,X,C):-
	b(X),
	{ C=c(2,X) with p([p(X,2),''],[0.2,0.8])}.



a(2).
a(1).

b(1).

/*p(1,2):-
	choice(1,2),choice(2,1).
	
p(1,X):-X\=2,choice(1,X).

p(X,2):-X\=1,choice(2,X).	
*/
p(X,Y,T):-
	(setof((1,2,1,Y),V^choice(1,Y,V),L1),X=1->
		(setof((1,2,2,X),V^choice(2,X,V),L2),Y=2->
			append(L1,L2,L),
			build_lists(L,_LX,LV),
			build_table(L,Tab),
			{T=p(X,Y) with p([t,f],Tab,LV)}
	;
			build_lists(L1,LX,LV),
			build_table(L1,Tab),
			{T=p(X,Y) with p([t,f],Tab,LV)}
		)
	;
		{T=p(X,Y) with p([t,f],[0,1])}
	).


build_lists([],[],[]):-!.

build_lists([(_,_,R,X)|T],[X|TX],[V|TV]):-
	choice(R,X,V),
	build_lists(T,TX,TV).

build_table(L,Tab):-
	build_col(0,L,Row1,Row2),
	append(Row1,Row2,Tab).
	
	
build_col(0,[],[0],[1]):-!.

build_col(1,[],[1],[0]):-!.

build_col(Active,[(H,NH,_,_)|T],Row1,Row2):-
	listN(NH,1,LH),
	cycle_values(Active,LH,H,T,Row1,Row2).

listN(0,_,[]):-!.

listN(N,I,[I|L]):-
	N1 is N-1,
	I1 is I+1,
	listN(N1,I1,L).
	
cycle_values(Active,[],_H,Var,[],[]):-!.

cycle_values(Active,[H|T],H,Var,Row1,Row2):-!,
	build_col(1,Var,Row11,Row21),
	append(Row11,Row12,Row1),
	append(Row21,Row22,Row2),
	cycle_values(Active,T,H,Var,Row12,Row22).
	
cycle_values(Active,[_H|T],H,Var,Row1,Row2):-!,
	build_col(Active,Var,Row11,Row21),
	append(Row11,Row12,Row1),
	append(Row21,Row22,Row2),
	cycle_values(Active,T,H,Var,Row12,Row22).
