

/*

set(ground_body,true).
s([a],P).
Variables: [(1,[]),(2,[]),(3,[])]
P = 0.1719 ? ;

set(ground_body,false).
   ?- s([a],P).
P = 0.099 ?
*/

:- use_module(library(clpbn)).
:-use_module(library(lists)).

%a:0.1:-p(X).

/*choice(1,C):-
	p(X,V),
	{ C=c(1) with p([a,''],[0.1,0,
												0.9,1],[V])}.
*/
choice(1,X,C):-
	p(X,V),
	{ C=c(1,X) with p([a,''],[0.1,0,
													  0.9,1],[V])}.

%p(1):0.9.
choice(2,C):-
	{ C=c(2) with p([p(1),''],[0.9,0.1])}.

%p(2):0.9.
choice(3,C):-
	{ C=c(3) with p([p(2),''],[0.9,0.1])}.


a(T):-
	(setof((1,2,1,X),choice(1,X,V),L)->
		build_lists(L,LX,LV),
		build_table(L,Tab),
		{T=a with p([t,f],Tab,LV)}
	;
		{T=a with p([t,f],[0,1])}
	).

build_lists([],[],[]):-!.

build_lists([(_,_,R,X)|T],[X|TX],[V|TV]):-
	choice(R,X,V),
	build_lists(T,TX,TV).

build_table(L,Tab):-
	build_col(0,L,Row1,Row2),
	append(Row1,Row2,Tab).
	
	
build_col(0,[],[0],[1]):-!.

build_col(1,[],[1],[0]):-!.

build_col(Active,[(H,NH,_,_)|T],Row1,Row2):-
	listN(NH,1,LH),
	cycle_values(Active,LH,H,T,Row1,Row2).

listN(0,_,[]):-!.

listN(N,I,[I|L]):-
	N1 is N-1,
	I1 is I+1,
	listN(N1,I1,L).
	
cycle_values(Active,[],_H,Var,[],[]):-!.

cycle_values(Active,[H|T],H,Var,Row1,Row2):-!,
	build_col(1,Var,Row11,Row21),
	append(Row11,Row12,Row1),
	append(Row21,Row22,Row2),
	cycle_values(Active,T,H,Var,Row12,Row22).
	
cycle_values(Active,[_H|T],H,Var,Row1,Row2):-!,
	build_col(Active,Var,Row11,Row21),
	append(Row11,Row12,Row1),
	append(Row21,Row22,Row2),
	cycle_values(Active,T,H,Var,Row12,Row22).
	

p(1,T):-
	choice(2,V),
	{T=p(1) with p([t,f],[	1, 0, 
										  0, 1],[V])}.

p(2,T):-
	choice(3,V),
	{T=p(2) with p([t,f],[	1, 0, 
										  0, 1],[V])}.
