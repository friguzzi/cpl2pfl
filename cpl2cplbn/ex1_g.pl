
/*
   ?- p(1,2,T).
p(T=t)=0.2,
p(T=f)=0.8 ?
yes
   ?- p(1,1,T).
p(T=t)=0.19,
p(T=f)=0.81 ?
yes
*/
:- use_module(library(clpbn)).
:-use_module(library(lists)).



%p(1,X):0.1:-a(X,Y),c(Y).

choice(1,v(X,Y),C):-
	a(X,Y),c(Y),
	{ C=c(1,X,Y) with p([p(1,X),''],[0.1,0.9])}.


%p(X,2):0.2:-b(X).

choice(2,v(X),C):-
	b(X),
	{ C=c(2,X) with p([p(X,2),''],[0.2,0.8])}.



a(1,2).

a(1,3).


b(1).

c(2).

c(3).

/*p(1,2):-
	choice(1,2),choice(2,1).
	
p(1,X):-X\=2,choice(1,X).

p(X,2):-X\=1,choice(2,X).	
*/
p(X,Y,T):-
	(setof((1,2,1,v(Y,Z)),V^choice(1,v(Y,Z),V),L1),X=1->
		(setof((1,2,2,v(X)),V^choice(2,v(X),V),L2),Y=2->
			append(L1,L2,L),
			build_lists(L,_LX,LV),
			build_table(L,Tab),
			{T=p(X,Y) with p([t,f],Tab,LV)}
	;
			build_lists(L1,LX,LV),
			build_table(L1,Tab),
			{T=p(X,Y) with p([t,f],Tab,LV)}
		)
	;
		(setof((1,2,2,v(X)),V^choice(2,v(X),V),L),Y=2->
			build_lists(L,_LX,LV),
			build_table(L,Tab),
			{T=p(X,Y) with p([t,f],Tab,LV)}
		;
			{T=p(X,Y) with p([t,f],[0,1])}
		)
	).


build_lists([],[],[]):-!.

build_lists([(_,_,R,X)|T],[X|TX],[V|TV]):-
	choice(R,X,V),
	build_lists(T,TX,TV).

build_table(L,Tab):-
	build_col(0,L,Row1,Row2),
	append(Row1,Row2,Tab).
	
	
build_col(0,[],[0],[1]):-!.

build_col(1,[],[1],[0]):-!.

build_col(Active,[(H,NH,_,_)|T],Row1,Row2):-
	listN(NH,1,LH),
	cycle_values(Active,LH,H,T,Row1,Row2).

listN(0,_,[]):-!.

listN(N,I,[I|L]):-
	N1 is N-1,
	I1 is I+1,
	listN(N1,I1,L).
	
cycle_values(Active,[],_H,Var,[],[]):-!.

cycle_values(Active,[H|T],H,Var,Row1,Row2):-!,
	build_col(1,Var,Row11,Row21),
	append(Row11,Row12,Row1),
	append(Row21,Row22,Row2),
	cycle_values(Active,T,H,Var,Row12,Row22).
	
cycle_values(Active,[_H|T],H,Var,Row1,Row2):-!,
	build_col(Active,Var,Row11,Row21),
	append(Row11,Row12,Row1),
	append(Row21,Row22,Row2),
	cycle_values(Active,T,H,Var,Row12,Row22).
