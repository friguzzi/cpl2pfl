

/*

set(ground_body,true).
s([a],P).
Variables: [(1,[]),(2,[]),(3,[])]
P = 0.1719 ? ;

set(ground_body,false).
   ?- s([a],P).
P = 0.099 ?
*/

:- use_module(library(clpbn)).
:-use_module(library(lists)).
%a:0.1:-p(X).

/*choice(1,C):-
	p(X,V),
	{ C=c(1) with p([a,''],[0.1,0,
												0.9,1],[V])}.
*/
choice(1,C):-
	findall([p(X)],p(X,V),L),
	build_lists(L,_LX,LV),
	build_table([0.1,0.9],L,Tab),
	{ C=c(1) with p([a,''],Tab,LV)}.

/*	{ C=c(1) with p([a,''],[0.1,0.1,0.1,0,
													0.9,0.9,0.9,1],[V1,V2])}.
*/

build_lists([],[],[]).

build_lists([HL|TL],LX,LV):-
	build_lists1(HL,LX0,LV0),
	append(LX0,LX1,LX),
	append(LV0,LV1,LV),
	build_lists(TL,LX1,LV1).

build_lists1([],[],[]):-!.

build_lists1([H|T],[X|TX],[V|TV]):-
	H=..[F|Args],
	append(Args,[V],Args1),
	H1=..[F|Args1],
	call(H1),
	build_lists1(T,TX,TV).

build_table([P],L,Row):-!,
	build_col_body(L,f,P,1,Row).

build_table([HP|TP],L,Tab):-
	build_col_body(L,f,HP,0,Row),
	append(Row,Row1,Tab),
	build_table(TP,L,Row1).

build_col_body([],t,HP,_PNull,[HP]):-!.

build_col_body([],f,_HP,PNull,[PNull]):-!.


build_col_body([HB|TB],TruthPB,Prob,PNull,Tab):-
	build_col(HB,TB,t,TruthPB,Prob,PNull,Tab).
	
	
build_col([], TB, Truth, TruthPB, Prob, PNull,Tab):-!,
	(Truth = t ->
		build_col_body(TB,t,Prob,PNull,Tab)
	;
		build_col_body(TB,TruthPB,Prob,PNull,Tab)
	).


build_col([\+ H|T],TB,Truth,TruthPB,P,PNull,Row):-!,
	build_col(T,TB,f,TruthPB,P,PNull,Row1),
	append(Row1,Row2,Row),
	build_col(T,TB,t,TruthPB,P,PNull,Row2).

build_col([_H|T],TB,Truth,TruthPB,P,PNull,Row):-
	build_col(T,TB,Truth,TruthPB,P,PNull,Row1),
	append(Row1,Row2,Row),
	build_col(T,TB,f,TruthPB,P,PNull,Row2).
	

%p(1):0.9.
choice(2,C):-
	{ C=c(2) with p([p(1),''],[0.9,0.1])}.

%p(2):0.9.
choice(3,C):-
	{ C=c(3) with p([p(2),''],[0.9,0.1])}.


a(T):-
	choice(1,V),
	{T=a with p([t,f],[	1, 0, 
										  0, 1],[V])}.


p(1,T):-
	choice(2,V),
	{T=p(1) with p([t,f],[	1, 0, 
										  0, 1],[V])}.

p(2,T):-
	choice(3,V),
	{T=p(2) with p([t,f],[	1, 0, 
										  0, 1],[V])}.
