
:- use_module(library(clpbn)).



% a:0.1.
choice(1,C):-
	{ C=c(1) with p([a,''],[0.1,0.9])}.


%b:0.3;c:0.6.

choice(2,C):-
	{C=c(2) with p([b,c,''],[0.3,0.6,0.1])}.

%a:0.2:- \+ b.
choice(3,C):-
	b(V),
	{C=c(3) with p([a,''],[0,	0.2,
												 1, 0.8],[V])}.

a(T):-
	choice(1,V1),
	choice(3,V2),
	{T=a with p([t,f],[	1, 1, 1, 0, 
										0, 0,	0, 1],[V1,V2])}.


b(T):-
	choice(2,V),
	{T=b with p([t,f],[	1, 0, 0,
										0, 1, 1],[V])}.

c(T):-
	choice(2,V),
	{T=c with p([t,f],[	0, 1, 0,
										1, 0, 1],[V])}.

