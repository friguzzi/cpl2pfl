
:- use_module(library(clpbn)).

mother(m,s).
father(f,s).


cg(m,1,p).
cg(m,2,w).
cg(f,1,p).
cg(f,2,p).
%cg(X,1,A):0.5 ; cg(X,1,B):0.5 :- mother(Y,X),cg(Y,1,A) , cg(Y,2,B).

choice(1,C):-
	mother(Y,X),
	cg(Y,1,A),cg(Y,2,B),{C=r(1,X,A,B) with p([cg(X,1,A),cg(X,1,B)],[0.5,0.5]}.

%cg(X,2,A):0.5 ; cg(X,2,B):0.5 :- father(Y,X),cg(Y,1,A) , cg(Y,2,B).

choice(2,C):-
	father(Y,X),
	cg(Y,1,A),cg(Y,2,B),{C=r(1,X,A,B) with p([cg(X,1,A),cg(X,1,B)],[0.5,0.5]}.

cg(X,1,A):-choice(1,cg(X,1,A)).

cg(X,2,A):-choice(2,cg(X,2,A)).	
 

