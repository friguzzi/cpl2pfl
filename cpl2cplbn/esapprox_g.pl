

/*

set(ground_body,true).
s([a],P).
Variables: [(1,[]),(2,[]),(3,[])]
P = 0.1719 ? ;

set(ground_body,false).
   ?- s([a],P).
P = 0.099 ?
*/

:- use_module(library(clpbn)).

%a:0.1:-p(X).

/*choice(1,C):-
	p(X,V),
	{ C=c(1) with p([a,''],[0.1,0,
												0.9,1],[V])}.
*/
choice(1,X,C):-
	p(X,V),
	{ C=c(1,X) with p([a,''],[0.1,0,
													  0.9,1],[V])}.

%p(1):0.9.
choice(2,C):-
	{ C=c(2) with p([p(1),''],[0.9,0.1])}.

%p(2):0.9.
choice(3,C):-
	{ C=c(3) with p([p(2),''],[0.9,0.1])}.


a(T):-
	choice(1,1,V1),
	choice(1,2,V2),
	{T=a with p([t,f],[	1, 1, 1, 0, 
										  0, 0, 0, 1],[V1,V2])}.


p(1,T):-
	choice(2,V),
	{T=p(1) with p([t,f],[	1, 0, 
										  0, 1],[V])}.

p(2,T):-
	choice(3,V),
	{T=p(2) with p([t,f],[	1, 0, 
										  0, 1],[V])}.
